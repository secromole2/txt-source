
“是你，杀’死茜尔维亚的。”

声音低沉地仿佛匍匐在地。至今为止从未听过的声音。
明明是完完全全对着我讲的話语，却又像是对着别的谁在说一样。
我依旧是不明所以，呆住了，这时管家淡然告知了妹妹的死。

而事实上，一瞬间，我心中出现了抑制不住的喜悦。
这下就没人打扰我了，这下索勒鲁就会看着我了，我不用去看心心相惜的两人了。
脑海中掠过了这种美妙的幻想。

但是，就这样被惊喜到的，只有一瞬。

“果然，是你。”

他的声音将我拉回现实。
依稀听到的声音让我脸色苍白。（ざっと音をたてて血の気が引いた。）

我，到底，在想什麼啊。

那是我妹妹啊。虽然她确实是我的情’敌，但，她也确确实实是我的妹妹啊。
小时候，我握着她的小手，曾下定决心要保护这个有些体弱的孩子。
我發誓，为了这个孩子，我要做好“姐姐”的角色。
虽然我没有做得很好，虽然最终变成了快要违反当天的誓约的情况，但是，即便这样。
她是我的妹妹，我是她的姐姐啊。

那孩子，那个，茜尔维亚，死’了。

我口中發出了嘘嘘的喘气声。
脸色依旧苍白，脑中混乱一片，想要找谁求助般，心臟不停跳动。

“果然，是你，杀’的。”

索勒鲁盯着我的表情，仿佛确信了般，口中胡言乱语一片。
我轻声说到：“不对”。但我不清楚索勒鲁是否听到了。
我一遍又一遍地说到：“不对，不对。”但索勒鲁似乎听不见，他将手中的刀扔掉了。

“无法原谅，绝对，无法原谅。”

一直以来都黯淡一片的眼瞳中，清晰露出憎’恶的神色。
我嘴唇轻颤，打算说：等等，却什麼話都说不出来。
喉咙仿佛烧着了般炽热，感觉全部的話语都被夺走了。
索勒鲁仅仅瞥了那样的我一眼，桌子上散落着所剩无幾的晚餐，他一口气将桌布拽下。桌上摆放的全部东西都一下子摔到坚硬的地板上。

响声刺耳。

脆裂的花瓶和，装饰其中的各色花朵。
那些花，是我为了难得的二人进餐，亲手準备的。
为了不让过於忙碌的索勒鲁心情低落，我特意收集了颜色温和的花朵，注意既不要华丽但也不要樸素过头，用心装饰好。

大理石上散落的晚餐，是我从幾天前就和厨师长商量了一遍又一遍才準备好的。
感觉索勒鲁肯定累了吧，考虑了对胃温和，滋润的食材。

每件每件，都是有意义的。
每件每件，都是我再三斟酌才準备的。
我回忆着将视线移开，看到了被葡萄酒和汤浸染发黑的桌布。那是我为了难得的机会细细编织的新桌布。

被扔下的东西，杂乱成片，在大理石地上混成一团。
我不知道该幹什麼好，不由将它们拉扯过来。
我想要是他意识到这是新桌布的話，我会很开心的。因为索勒鲁是不会留意这些的人，但要是他注意到了的話，我该回答些什麼好呢。我甚至连这都在考虑，一个人浮想联翩。

阻挡了我的视线的，是一双崭新的皮靴。

明明平时走路不会發出响声，但这时他伴着重重的脚步声走了。

无视跌坐着的我，索勒鲁如今要走出房间。
等等，拜托，等等。
谁来，谁来，对他说说。我什麼都没做。
谁来告诉他，不是我做的。

全身都在叫喊，但是，一句話都说不出来。
仅仅不断呜咽着，一个单词都讲不出。

因为，我想都没想过。
索勒鲁，一直以来，是这麼认为我的啊。
我是杀’死’妹妹的人，他是这麼认为我的。

并不冷，但我不停颤抖。
我没有任何意义地紧紧抱住桌布，现在，没有任何人能够拥抱我。
我仿佛抽'搐般吸气，这时被谁抓住了手腕。
从两旁拉起，吊着般让我站起来。

就像是，罪’人。

索勒鲁，索勒鲁，对您来说，我就是，这样子的存在啊。
至今积累起来的日子，度过的时间毫无意义。
连我的辩解，都不愿听啊。
无法言出的想法，化作呜咽于嘴中凋零。
追赶着，背後被推着离开，双腕被粗’暴地抓住。

砰！地，关门声巨响，就像在表示索勒鲁的拒绝。
连头都完全没有回。
妻子的叫喊，哭泣声，都全然没有阻止他的离去。

——於是，我和索勒鲁就此诀别，没再走近过第二次。

那是，我最初的人生。

那之後，我被软'禁在自己房间，管家告诉我，只要收集好了证据，索勒鲁就立刻和我离'婚，将我上'交国’家。
我申诉着自己的无罪，心想绝对不会变成那个样子的，即便被软’禁了，我也相信索勒鲁会想明白。
毕竟，我和茜尔维亚的死无关。

但是，很奇怪地，不久我就作为罪'人关进监’狱内了。

被铁'栅’栏所困身子难以动弹，一个个证据被提出，不知不觉身上就背负了重重罪’状。
据说，我从未遇过从未见过的强盗团一伙儿，他们已经坦白称是受我指示，才去袭’击伯爵家的马车的。我听了，不由自主地笑了出来。
这些滑稽之谈，索勒鲁居然让“国'家”相信了。

当我发觉，自己不知被谁陷’害了的时候，已经，变成了这种状况，我背负上了杀’害亲属的罪’名。

我虽然知道，在贵族之间，时不时会有像这样的陷阱在。但没料到会让自己深陷进去。

毕竟，甚至不用想就知道，其他人对下代侯爵夫人的我的身份，是渴求到了哪一个极致啊。
我也，期望着自己的身份。
原本，我的話，在索勒鲁的妻子的立场上，是什麼都无需顾虑的。
这麼一想，会有其他人希望换掉我也毫不奇怪。

那麼，就将事情引导成那样吧。
这很简单。排除碍事的傢伙就行。

我原来打算万分小心的。
但是，考虑得不够充分。
我想都没想过，自己会被这种手段给剥夺掉所有。

在自己不知道的地方，“伊莉雅”这麼一个人正在被人不断诋’毁。
既然被关进监狱了，我做不到用自己的双手来证明自己的清白。
能做到的，只有，祈祷而已。
祈祷着，有谁能，证明我的清白。

我就这样，在最後的最後都祈祷着。
而且，相信着。
有谁会，有索勒鲁会，将我从这个牢’狱救出。

我跪在布满裂缝的石头地面上，手膝枕上生来从未目睹过的粗糙床，捧手祈祷。
索勒鲁是“正确的人”。

不，是期望着成为正确的人的人。
是明辨是非的人。
如今他只是被茜尔维亚的死动摇了，双眼蒙灰而已。
冷静下来就能明白。
他会明白收集到的证据都是伪造的。
所以，他肯定能够，证明我的清白。
即便现在不行，终有一日，我会迎来洗脱罪’名的时候。
我这样坚信着。

因为我爱着这个人。

为什麼，要到这种地步。

为什麼，要相信他到这种地步。或许有人听了会觉得在意。

我也不知道。
我觉得，并没有所谓答案。
只不过，我爱他。
就要發疯那般，不，发了疯那般爱着他。

——但是，结果，我坚信的他，没有来救我。

我不记得自己的最後了。

没有被处’刑了的印象，所以肯定，是就这麼死’在牢’狱里了吧。
我回忆得起散發着霉臭味的那个地方。
作为贵族千金而出生，将次代侯爵夫人纳为己有的贵人，最後死’在了那麼残酷的地方。
更别提这是冤’罪了。
所以我肯定，在那种地方活不下去的吧。

本来的話，收’容犯’了’罪的贵族是在别的地方的，与牢’狱这种从头到尾散發着不洁的地方无缘。
要是按照正规的手段，我也应该是被关到其他地方的。
但是，我的，亲生父母不允许，索勒鲁也不允许。
索勒鲁是次代侯爵，侯爵家的第一位，也就是公爵家的次席。
也就是说，刚低于王族的立场。
他只要出声了，大抵都能成。
他清楚这些，所以他也严于律己。
那个索勒鲁希望将我放进牢’狱’裡。
我就是被他憎恨到这种程度。

所以，我不是作为贵族，而是作为一个平民，被裁’决了。
这个世界上唯一的，我认为是站在我这边的，父母，他们也抛弃了我。在那个瞬间，我的人生肯定真正意义上的完结了吧。

茜尔维亚被谁都爱着。
连双亲也，比起我，更爱茜尔维亚。

这个世界是以茜尔维亚为中心运作的。
既然这麼说，那茜尔维亚死'后，就仅仅是迎来了尾声。
故事的附录，只是追记而已。

并不是什麼重要的事情。

那麼，我的死，对这个故事来说，肯定是怎麼都好的事情吧。

