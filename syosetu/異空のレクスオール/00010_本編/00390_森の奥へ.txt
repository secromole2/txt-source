
奔跑、奔跑、奔跑。
要和艾莉婕，在森林中全力地奔跑着。

“那、那些是什麼啊……！”
“不要停！会被吃掉的！”

在两人身後追赶着的，是无数黑色团块的集群。
有着包裹着锃亮发光的黑色铠甲般的六只脚，头部也有着看上去相当强韧的尖牙。
如果从上空向下看的话，那副身姿，谁都会自然而然地说这是“巨大的蚂蚁”吧。
而这样的集群，如今就像要淹没树木间缝隙一般紧追在要他们的身後。
名称是，装甲蚁。
单只的话有着人类成人的大小，和刚才的巨人相比可能算小，但硬度却是不能比拟的。
甚至被形容为穿着着金属铠的装甲蚁相对的，动作相当钝重，但也是有着一旦遇见难以狩猎的敌人就会呼唤同伴这一相当麻烦特性的怪物……更糟的是。现在这些离开了地下城的装甲蚁们充分过头地集合起来，正慢吞吞地开始和要他们玩起了追逐遊戏。

“用！艾、艾莉婕的魔法！能、能不能想想办法！？”
“你说对那种东西产生效果！难道是说要使用吹飞整个森林级别的魔法吗！？半吊子的魔法连停下他们的脚步都做不到啊！”
“其实！就算吹飞！也没关係的吧！”
“诶诶！是这样啊！但这也是有敢在魔法发动前一直站在那裡看它们冲过来的胆量才成立的说法！”

没错，大型的魔法当然也需要相应的时间和集中力。
想使用将追赶着的装甲蚁们全部打飞的魔法的话，仅仅一瞬间的空隙实在是不够用。
但就算这麼说，如果去慢悠悠地準备魔法的话，明显被装甲蚁把头咬成稀巴烂来的更快。
因此才这样拼命逃跑试图甩开它们，但是装甲蚁们却一点放弃的样子都没有。

“要大人才是！没有能打破这种局面的魔法吗！？”
“唔……我、我试试！”

当然了，对要来说，射出箭矢的话就有转身的必要。
转身，搭弓，射箭。
这个过程也是需要一定的时间的，而现在并没有拉开那麼远的距离。
……这样的话，就需要就这麼面向前方射出箭，还能射中对方的相当破天荒的攻击。
但是要现在还不清楚“哪种箭矢有着什麼样的能力”。
也就是说，只能拿看上去可能有这种能力的箭矢一个个地试过去了。

“矢作成·无法逃离的风之矢！”

要伸出手，从空无一物的空中抓住一根半透明的箭矢，要就这麼一边跑着强行架起弓。

“拜托了……可要起点作用啊！”

就这麼将视线固定在前方，要拉弓射箭。
紧接着箭矢描绘出难以想象是以这种不安定的姿势射出的轨道……就这麼划着複杂的曲线，正面刺进了要在射出瞬间看着的那根树中。

“……是这种箭啊！”
“去射树是要做什麼啊！？”
“我、我知道！那个……矢作成·穷追不捨的猎犬之矢！”

要抓起手边掉落的浮土咏唱道，土壤在要手中变成了浓茶色的箭矢。

“木头什麼的就算了……去对那些蚂蚁想想办法！”

在脑中浮现出刚才一瞬撇到的装甲蚁凶恶的面容。要射出了箭矢。
果然明明是以跑动中不安定的姿势射出，但箭矢却快速地离开要的弓……就这麼穿过要的脚边，朝背後飞去。

“哦……！”

片刻後，有什麼被打碎的声音传来，同时也能听到那个东西倒下的声音。
但是紧接着，这东西被踩碎的声音也一併传来，从这些声音就能清楚地理解到，看来只被打倒了一只。

“……呜哇。这群傢伙明显是把同伴给踩了”
“不是说这种话的场……！”
“矢作成·穷追不捨的猎犬群之矢”

来不及回应艾莉婕的抗议，要用手抓住新的箭矢。
该用哪个“系统”已经清楚了。
这样的话，就没有问题。

“……去吧！”

紧接着射出的箭矢再次飞向要他们的背後……就这麼在上空，分散成无数的小箭倾泻而下。
不过虽然在视觉上可以说是“倾泻而下”，但却并不是事实。
这些箭矢分别射中不同的装甲蚁，将其变成了尸骸。
果然就算是装甲蚁们，在眼前倒下的尸骸构成的障碍物前还是不得不停了下来。
脚的构造上无法进行“跳跃”的装甲蚁们，果然在这种规模的障碍物前，还是很难做到“凭借势头将其碾碎”。
紧接着……装甲蚁们停止进军的事实，就由声音传给了要两人。

“如、如果会用这种魔法的话……就不能……一开始就用吗……！”
“嘶、哈……这、是有理由……唔咕”

虽然一停下来，各种疲劳就一下子反馈给要他们……但要他们也还没傻到，觉得现在这个瞬间能慢慢休息。

“……那，艾莉婕。再问一次。艾莉婕的魔法对那些，能有办法吗”

对用着手甲在头上擦汗的要，艾莉婕也将因汗贴在脸上的头髮捋起，回答道。

“这还用问吗，要大人。正好现在也有些热了……就给您展示一下我压箱底的冰魔法吧”

紧接着，完全不知“撤退”留在原地的装甲蚁们……被艾莉婕施展的超广範围的冰魔法，化成了巨大的冰壁。
