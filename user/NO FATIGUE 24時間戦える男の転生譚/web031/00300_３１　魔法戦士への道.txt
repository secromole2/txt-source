   ――此後，我淹没在父母和司祭的问题攻势中。

 　关於【不易不劳】的性质。
 　关於【鉴定】的性质。
 　关于格雷萨。
 　< 八咫鸟 >。
 　关於魔法和技能。
 　关於前世的记忆。

 　我，只要是自己知道的信息就决定共享。

 　首先，MP最大值的提高方法。

 　――持有魔法系的二个名，有从神保佑的人，到把MP使用到完然後晕倒的话，每回最大MP上升1。

 　这个，因为从女神桑那听到了，绝对没有错儿。

 　朱莉娅妈妈因为有《炎狱的魔女》的第二个名，用完MP如果气绝的话最大MP应该提高。

 　不过，因为与有【不易不劳】的我不同，妈妈气绝每次需要3个小时的成长眠，效率相当差。
 　尽管如此如果每天睡之前都持续的话，一个月大概最大MP提高30。
 　如果有30的MP，《火焰长枪》能打3发。

 　妈妈听我了的话，眼睛闪耀着光芒，

 「那个，很厲害哟，埃德加！　从今晚开始睡之前必须要用完MP！」

 　感到喜悦。

 　格雷萨战後，成长眠时候从女神那得到了【附加魔法】的技能，司祭盯住了我。

 「阿斯特拉大人，是怎麼样的!?」

 　用揪起来的气势问我。

 「是黑色的头髮，和善漂亮的人吗」

 　我那样回答着，

 「呶。确实，神殿的传说中，阿斯特拉是被认为女神。普通的一般认为是男性……」

 　是吧，嘟哝着。

 「那麼说来，爸爸和妈妈，也可以接受下受托。
 　鉴定後，技能栏也有很多技能呢」（译者：じゅたく をうけた ほうが いいよ）

 「什麼！如果那样，请立刻做」

 「……问埃德加也可以的说」

 「鉴定的话还是等下再说吧」

 「埃德很慎重。那个也是因为前世的知识吗？」

 「嗯。多数样品还是去比较下好」（译者：サンプルは おおい ほうが いいって）

 　关於转生，现在说也不太容易懂，结果司祭说的拥有着「前世的知识」的，还是镇定下来了。
 　我，已经有着30岁的魂之类的，毫不隐瞒地说出的勇气也没有。
 　这个秘密决定带到坟墓裡。

 　尽管如此，没有必要掩盖状态，感到负担变轻了顿时泄了气。
 　多亏知道前世的知识的，稍微有点奇怪的事也不觉得奇怪了。

 「样品？」

 「嗯哆……Rei，那样的感觉」（译者：这个是朱莉娅说的吗。）

 「例子、假名。确实是那样」（译者：例、かな。たしかにそうだ）

 　在说着话，爸爸，妈妈正在司祭那做天啟。

 　结果――

 「领受了玛鲁萨鲁多大人的关注！」
 「做到啦！【火魔术】等级9！得到了《火精的保佑》！」

 「耶！」「做到啦！」一反常态地两个人，父母父母心情很好的击掌着。

 　拿来饮料的佣人（马尔苏拉），大吃一惊的看着挽着胳膊欢乐的跳着的子爵夫妇。

 　看着那个也没有办法，我对司祭试着打探。

 「适性的标准不能说出来吗？」

 「噢，【鉴定】後也知道那个吗？
 　了不起呢。
 　可是，我在意着，从【鉴定】知道的事情，不能随便地说出。
 　理由，明白吗？」

 「因为伤了别人的心情？」

 「不只那个，问题不仅仅停留在这点上。
 　我看过的职业很多，到现在为止也有幾千个程度的状态。
 　作为那个结果，状态上有秘密的人意外地多。
 　如果一般的认为那个秘密被人知道，根據情况也可能有人为了防止泄露而封上埃德加的嘴。
 　所以还包括【鉴定】，尽量不让别人知道。
 　――哦」

 　司祭稍微切断言词，看了爸爸们。
 　因为这边开始认真的话，雀跃的爸爸和妈妈也返回到老老实实的脸。

 「关於传说级以上的技能，不能向家人以外的人说出来。
 　【鉴定】不仅仅是别人怕泄露自己的秘密。
 　只那些稀少的技能，要是複数持有着，也会招致人的嫉妒。
 　还有，接近你打算利用的傢伙也会很多」

 「……是」

 　我前仰后合的点头了。

 「因此，适应性诊断的话。
 　当然的，请让我做。
 　神赐福的孩子埃德加的适应性怎样组成的，我自有的有兴趣」

 　司祭那样说，半闭着眼。

 　把双手转向上面，简直象用秤一样什麼的。

 　暂时等候。

 「这个孩子……诶」

 　司祭那样说着沉思了。

 「怎麼样？…… 埃德加到底朝向什麼？」

 　爸爸用一半不安一半期待的感觉问着。

 　司祭点了下头之後，慢慢地这样说了。

 「对於埃德加，没有特别优秀的才能」

 　在那个场合全体人员摔倒了。

 「不不，不是很坏地说。
 　特别卓越的才能――那样的说，象朱莉娅桑的火属性魔法和彻斯特的弓的才能，看起来好象没有什麼不擅长的，另一方面，也没有特定的朝向」

 「没有特定的……吗？」

 　爸爸说。

 「还有。恐怕，魔法也好武艺也好，这个孩子不会风平浪静着。
 　在我知道的领域，埃德加有着普通人以上的才能」

 「那，是新奇事吗？」

 「新奇。我第一次看了这样的适应性的持有者。
 　恐怕，与有阿斯特拉大人的加护有关係……」

　 那个是说准了，司祭先生。
 　试着看我的《善神的加护+1》的求助信息。

 《掌管魂的轮回的女神阿斯特拉的加护。促进魂的成长。全技能的学习条件解放，技能的学习·成长中补正。》

 　是有【不易不劳】，没有女神桑的关怀技能也无法学会这麼多。

 「……原因上，是全体都一样的意思？」

 「并不是说，完全一样。
 　只是说适应性的偏差幅度不超过常识的阶段。
 　朱莉娅桑和彻斯特，或埃德加的2个哥哥，并且，当然还有阿尔弗雷德桑，也有卓越的适应性的领域，相对的其他领域无论如何也很难对付，无论怎麼做，也不能学会那个领域的技能。」

 「爸爸的弓之类的？」（译者：男主学会技能【暴击】！）

 　爸爸呆然的愣住了。

 「嘿嘿嘿。那样的话。
 　顺便，如果举出埃德加的适应性中高的地方，首先是【雷魔法】」

 「雷电魔法……」

 「【雷魔法】是近幾年被开发的魔法，有着弹速很快，使对象麻痹的效果的，期盼学会的魔法师很多，拥有适应性的人却非常少。
 　实在是，魔法能发动的那样具体地联想倒『雷』，是非常的难。
 　埃德加，恐怕有天性的东西在，这个『雷』的印象应该很擅长的吧」

 「印象……」

 　司祭称为天性的东西，是从，在各个地方使用电的现代日本转生的原因。

 　总之，不是天性上的转世吗？

 　……我嗯，对自己想着的东西低落了。

 「象朱莉娅桑一样的，如果与超第一流的魔法师的适应性比较显得相形见绌，与岂只二流的魔法师比较，倒不如适应性很厲害」

 　……那，有充分的适应性？

 「抱歉哝。徹贝尔子爵家怪物成堆了吧？　而且，提到神的加护的持久性，不期待是没有的」

 　那样说的吗？

 「武艺怎样？」

 　爸爸。
 　正因为自己是武士，才会在意吧。

 「嗯。投掷武器普通，弓，长枪，然後铳，那麼」

 「投掷武器从弓，长枪……那样，最後哎呀？」

 「铳，什麼的。我也详细的说下吧，太古时期存在的独特的发射机构像弩一样的东西。稀少地从遗迹被发掘了，弹射构架好象用现代的技术不能制造，也不能满足使用」

 　我听「铳」有了兴趣，爸爸好象丢失了兴趣不太满意。

 「从远方的远距离攻击的武器擅长，吗？」

 「那样说没错。
 　以後，所说的是什麼『投』『飞』『弹』的能力的话。
 　与长物投掷，这两块拥有着说是一流也不为过的适应性。
 　有关长枪，比不上阿尔弗雷德殿下了吧」

 「近身战呢？」

 「……稍稍迟钝」

 　真过分！（译者：你刚才也暴击了你老爸的）

 「最好的方式，还是用长枪一边远距离攻击，通过魔法决定的组织战鬥的能力，这样的风格好。
 　因为近距离殴打没有这方面的状态，如果被接近还不如保持距离那样无灾无难。
 　当然，是有着普通人以上的才能，如果专心于近身战的练习，也能做得很好」

 　如果用格闘遊戏比喻，是弓兵造型吗？
 　别从远方用远射武器一边牵制消减对方的体力，对方现出间隙的时候使用威力高的攻击，这样的说。
 　与阿尔弗雷德爸爸的战鬥风格也相似。（译者：完全不像好吧！远射武器还能使用物理魔法和附加魔法）

 「魔法的资质，不像朱莉娅桑所达到的，凌驾着普通的魔法师着，没有特别难对付的属性。
 　不像朱莉娅桑专精一个属性，按照状况分开使用各种各样的魔法好。
 　当然，一生深通百艺也很难，磨练出成为核心的属性。
 　不过，幸运的有着【雷魔法】的适应性，这个就行了。
 　可是……fuumu」

 　司祭一边说一边沉思，

 「……这样试着考虑的话，这是可怕的才能。
 　确实与一艺优秀不同，死角都不找到。
 　一般，如果有了武艺的适应性就没有魔法的适应性，没有魔法的适应性就有武艺的适应性。
 　如果剑技能优秀弓技能很难应付，火魔法擅长水魔法不能学会，一样地，弱点潜藏在适应性的背面的。
 　稀少地兼备魔术和武艺的才能的人也有，不过一般的说把重点放在哪一面，另一面就相对的弱点。
 　埃德加还年轻。年轻的话。从现在开始，以自己的意思练习……或者……」

 　司祭再次沉思之後，说了。

 「以――魔法战士作为目标，也不好说」

 「魔法宣布……！」

 　是真的男生的言词。

 「嗯。倒不如说，阿斯特拉大人的意向也有可能。不然，也不会将这些的加护给予吧」

 「艾德……魔法战士……」

 「埃德加，魔术战士……」

 　爸爸和妈妈，亮闪闪的说着。
 　没问题吗。
 　爸爸&妈妈会不会突然进行斯巴达式的教育？

 「自报名为魔法战士的人本身也有。
 　特别是在炫耀学识的心气很强的冒险者也是。
 　但是，以真的意义称作魔法战士的人，与恶神摩卢恩韦苏作战，打成平局了这样的第一代《勇者》阿鲁班以外，只有数名在」

 　哦，勇者！
 　而且，与恶神作战才平局!?

 「口传的，所谓魔法战士，不仅仅是指自魔法和武艺单独通晓到极致的人。
 　――是能彻底弄清楚魔法和武艺完全不同的2种策略方法，同时提高『双方』那2个，能够得到被遗忘的传说级技能，那样被传达」

 「哦，噢！……」

　 危险。
 　我的心中的男孩子的部分奋起了。

 　想着这个世界，在遊戏裡被看做职业的概念好象也没有，这个所谓魔法战士，也不就是所谓的高级职业（完全掌握複数的大师级技能）一样的东西吗？
 　做着複数的技能锻炼，是有可能根據技能的组合学会新的技能。

 　那样考虑，偶然想出。

 「司祭。关於技能的学习，是有什麼还不知道的吗？」

 「……学习路径。
 　不详细地说下吗？」

 「做技能练习的话，奖励技能也能获得。
 　但是，对於那个奖励技能，不是以技能点的方式获得。
 　妈妈，现在得到了（【火精魔法】），之前就一直使用着魔法（【火魔法】）。
 　另一方面，爸爸，因为练习着(【长枪技】)，作为那个奖励有着(【长枪术】)」（译者：有点疲倦了，这段稍微粗翻，没有详细的查词的意思，但是意思上明白就好）

 　我那样说明，

 「确实，我是那样，但也不一定是那样。
 　有着【长枪技】的，没有【长枪术】的长枪使用者也多。
 　反过来，从【长枪技】的等级不到9级的时候学会【长枪术】的人，稀少地好象也有」

 　爸爸那样补充。

 「我，有着状态上《炎狱的魔女》的第二个名增加时，一起也获得【火精魔法】吗」

 「……技能的学会，有各种各样的路径，在司祭们之间也被知道。
 　但，我们根據向轮回神的誓约而被束缚，不能交换信息来验证的。
 　当然，从我个人的经验，像这样的假设也有些」

 　司祭干咳之後开始说。	（译者：司祭は「うおっほん」と咳払いしてから話しはじめる）

  「首先，【长枪技】对【长枪术】，还有【火魔法】和【火精魔法】的学会路径很易懂。
 　同样的，各种各样的适合武艺，各属性的魔法。
 　稍稍特殊的，投掷系的技能的话。
 　投刀的高手和投斧、或投标枪的高手的一部分的人，获得所说的【投掷策略】，同样的是达人级技能。
 　埃德加，不久也能获得这个技艺的」

 　是的。
 　现在还不知道那个。
 　格雷萨，也有着【投枪术】，不过，没有【投掷策略】。

 「同样的事，关於各种各样的技能是怎麼验证的。
 　怎麼学会技能，以战鬥为生的人们也不会说出来的。
 　不好意思的向埃德加提出过分请求，如果明白了学会的路径，能告诉我的话就很令人感激。
 　当然，不是免费。
 　请让我準备的适应的报酬」

 「明白了」

 　这样回答，我突然注意到了。

 　麦露薇很老实。

 　那样想时看着旁边的话，麦露薇显出难看的脸，关注着厨房的门。

 「……怎麼了，麦露薇？」

 「唔～～～嗯……，气息的原因，的吧？」

 　我，感到疑惑的打算对麦露薇搭话，不过，

 「埃德」「埃德加」「埃德加」

 　被好奇心高涨的3个大人们捉住，就变成了那样。

 　此後，我从3人的问题攻势中摆脱不掉。

 　由於【不易不劳】的效果不会感到疲劳，那边有着山一样多的想听的事，也因此成为了有意义的时间。


 　――可是，幾个小时後，我大大地後悔了，今天没有从麦露薇那打探出她说的话。

