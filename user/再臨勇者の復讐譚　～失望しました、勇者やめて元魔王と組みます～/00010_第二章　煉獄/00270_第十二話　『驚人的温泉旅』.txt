
　完成报仇的第二天。


　我起床後洗脸。
　好象没有完全消除疲劳，镜子中的我是一副疲倦的表情来着。


「早上好，伊织」

「啊，早上好」


　从楼阁下来时，艾尔菲已经起来着。


「心情怎样。冷静下来了吗？」

「……为什麼这麼说？」


　我并不没有不舒服。
　艾尔菲你在说些什麼啊。


「因为从昨天开始就相当忧虑。放鬆下来了吗？」

「…………啊啊」

「那就好」


　在那样的会話後，我们吃完早饭，从旅馆出来。

◆

　然後我们来到冒险者公会。
　是为了参加『迷宫讨伐队』的应试。


　即使没有成为冒险者，在这个应试裡及格了也能参加讨伐队。
　这是为了不管怎样都想进入讨伐队，不过又嫌成为冒险者麻烦的，任性的实力者而设的制度。


　要合法地进入迷宫，除了这个手段以外就没有其他了。
　马文的预先安排让我没有办法成为冒险者。
　我记住和那个傢伙有联繫的公会职员的名字，一旦有必要就敲-诈。


　向行会姑娘做应试的申请，今天是截止日期。
　看来，时间相当急。
　明天进行考试，然後好像马上就进行包含合格者在内的讨伐会议。


　总之，要提交写上名字的应试的纸张。
　冒险者登记的时候也是那样，不过，用『伊织』和『菲尔路』这名字来。
　为慎重起见，曾做了打算要用『奥利』与『波露菲』的提案，但艾尔菲做出了看起来非常非常讨厌的表情，结果用了这个名字。


「哼……」


　在公会中，与全身铠甲的男人擦肩而过。
　从头盔的间隙裡，他用尖锐的目光怒视着我们。


「…………」


　……需要警戒啊。


　然後弄完了事情，打算离开公会时。


「伊织先生！艾尔菲小姐！」


　似乎由於是跑着来的缘故而呼吸缭乱的米莎打招呼。


「……谢谢」


　然後，她深深地垂下头。


「……怎麼了，米莎小姐」


　因为决定要匿名，预先装糊涂。
　败露了变卖这一件事不会免得惹来什麼麻烦，但为慎重起见。
　米莎好象发现了，虽说我这边在装糊涂。尽管如此她说了好多次道谢。


「老先生的伤已经被完全医好了。只是为了慎重起见，还要住院一日，从明天开始就能出院……！也不会残留後遗症！」


　浮着泪光，米莎那样说了。


「但是……我不能收安排魔术师，和重整店面、维修的钱」

「即使说钱什麼的，我们也不明白」


　说是希望返还，不过，我装糊涂。
　花费相当长的时间来结束这个話题。


　结果，祖鲁茨的店决定要重整了。
　到那为止的时间裡，好像祖鲁茨和尼安尔鲁到和冒险者有关联的别处的铁匠那工作.
　好象成为那个正好腰疼的锻造手的替代。


　米莎到最後还是向我们垂下头，不知不觉说了“这个恩必定还”的話好多次。


　那个心情是真的吧。
　至少，现在是呢。
　但是为了自己，谁都会把恩情完全地捨弃并简单地背叛。


　所以我已经决定不再期待了。

◆

　把公会的事情弄完之後，买出在货摊子裡看起来好吃的东西，一边吃一边走在街上。


「白色部分会噗噜噗噜地抖动，黄色的部分会黏糊地化开。不可思议的食感之外又加上甘咸的调料汁，提拔着蛋的滋味。温泉蛋好美味」


　艾尔菲唠唠叨叨地谈着感想。
　好像舌头上都有。
　魔王，说不定是平民的舌头。


「於是伊织。你昨天所说的所谓『布鲁托加』是怎样的傢伙？你的反应就因为那种傢伙吗*」

【译：不知咋翻：お前の反応からして、またロクでもない奴みたいだが】

「那个傢伙，是迪奥尼斯的部下」


　被称为"炎鬼"，在鬼族中是擅长火焰的魔术的男人。
　不如迪奥尼斯，不过，尽管如此也是相当的实力者。


「是个对强大的傢伙谄媚，对比其弱的又十分强势的傢伙」

「那个还真是典型」

「最初见到时，我也被找碴得相当厲害了。我也显出力量，但是只是简单地还手罢了」


　但是，当时的鬼族有让人同情的部分。
　我虽然认为他是讨厌的傢伙,不过还是没办法地接受。
　现在如果回想起来，只能说我太傻了。


　与菲尔路一起，走向在温泉城市的背面的胡同。
　一小时过去，探索到目标後走过去。
　和在目标的店外边站立的独眼的男人谈話，进入其中。


　在马文的书房裡看过的资料，也有关於这条街的『里职业』的信息。
　我们为办理危险的药的店而来。


「臭……」


　为店内头充满着一种过火的臭味而头痛。
　菲尔路皱眉，捏住着鼻子。


「欢迎。是……相当年轻的客人」


　带着可怕脸色的店主像觉得可疑一样看过来。
　露出钱，马上就浮起卑下的表情并听話了。


「有鬼之爪这个药吗？」


　被问到那个，店主眯起眼。


「……鬼之爪啊，好像在相当可恨的共事者那。

　你运气真好。正好三日前左右得到的」


　翻查着嘎吱嘎吱作响架子，店主拿来放入透明的袋的粉末。
　是一种不光滑的白色结晶，外观很像糖。
　是可以在鬼族住的地域附近採取的，魔素结晶化的物体。


　向店主支付了钱，领取鬼之爪。
　为了严格地封密，预先放在口袋的裡面。


「啊，说起来啊，伊织，鬼之爪，大概是毒吧？」

「啊啊」


　鬼之爪是剧毒。


　如果摄取的話，会發生简直像在体内有小鬼乱跑乱闹一样的剧痛，所以用这个名字称呼。
　如果常人摄取的話，激烈的剧痛会变得无法活动，不久就会死。
　更加实质的坏处是，在摄取这个毒的状态下使用魔术的話，管理魔力的器官会失控，有着肉体会破裂这样的可怕的效果。
　因为时魔素结晶化的产物，对魔力量多的人，魔力的强的人某种程度有控制的效果。


「我被称为伙伴的那些傢伙灌下这样的毒」


　在读取的流泽苏的记忆中，也有贝鲁托加的身姿在。
　在怎样杀死我作战会议中，那傢伙说了。


『为了确切地杀了阿马茨，俺有策略』


　那个策略，是让我喝鬼之爪这样的东西。


「…………，不要紧吗？」

「幸运的是，因为有勇者的力量的缘故吗，只有让身体发倦的效果。是疲劳积压，健康状态不良的程度」

「……那样啊」


　来这条街的魔王军的手下，不知道是否是我所知道布鲁托加。
　如果要是那样的話，用这个鬼之爪杀。


「……伊织」


　考虑那样的事时，被菲尔路“啪”地打了肩膀。


「……怎麼了？」

「去温泉」

「什麼……？」


　为何现在说的話题会变成那样。
　菲尔路用「你不懂吗」这个表情说。


「因为好不容易来温泉都市，所以不泡温泉怎样行。旅馆配置的浴室已经厌倦透顶了」

「……那个啊」

「从现在开始，旅馆变更为有温泉的旅馆」

「等，哎……」


　被菲尔路的荒唐的力气拖走。
　真的，与这个傢伙在一起的話步调会被打乱……。

◆

　正因为叫温泉都市，在这条街上有很多的温泉旅馆存在。
　菲尔路把我带来她鉴定的其中一个。
　取消今天以後的投宿，带着行李来到那。


　这是一个很大的旅馆，也有着清洁感。
　进入其中的話，被像女仆一样的女主人迎接。
　要说到旅馆，就有要是和服的印象，不过在这个世界大部分洋式。


　被女主人接领，来到服务台。
　必须选择投宿的房间，晚饭和早饭，温泉的使用等的菜单。


「拜托两个一人房间」

「饭是两餐」


　我机智地表示关於饭的设定。


「温泉的使用要怎样做？」

「入浴时能要包场吗？」

「……包场会怎样」

「没有怎麼样。你能与素不相识的人一起洗澡吗？」


　嘿，也不是不明白那个心情，不过……。


「对不起，不能包场」

「唔」

「不过，有所谓的家族浴，可以以接近包场的形式利用温泉」


　根據女主人，好象有提供二人以上的客人的温泉。
　预先被决定使用时间，不过据说，在这期间，只有是家族成员才可以泡温泉。


「……二人进入不好吗？」

「我不介意」

「俺介意」


　不是包场就可以吧，菲尔路没有放弃的这样说。
　虽说再寻找其他的旅馆看看，菲尔路就说着「这裡就好！」地撒娇。
　结果，使用家族浴。


　进入各自的房间。


　因为今天也没有特别想做的事，决定在房间裡放鬆。
　在一只手上用另一只手使用魔技篡夺(spell·divider)进行着魔术威力的调整时，门被敲响，女主人进来。


　好象是晚饭的时间。
　与菲尔路一起去食堂时，饭菜已经被準备。


「噢……」


作为晚饭出来了的是火锅。

　把肉和蔬菜放入石锅，用像岩浆一样的红色的汤咕嘟咕嘟地煮。
　好像被称作岩浆火锅。


「这种辛辣是无法抵抗的！」


　菲尔路一边沉重地呼吸，一边贪婪地吃岩浆火锅。
　白天吃了那些，还能再吃？


「我大量地进食是有理由的」


　似乎是发现我的视线，菲尔路说出了抗议。


「要保持我大量的魔力，需要很多的能源」

「总而言之，每取回身体其他部分得話，饭量会变得更大吗？」


　如果取回全部身体，到底要吃多少啊。
　或者说，你只有头，你说说吃的东西哪裡去了？
　太神秘。


「如果取回身体，就变得能调整花费的能源」


　一边看起来很困难地说着，一边也停不下用来吃岩浆火锅的手。
　我变得担心真的没问题吗。
　吃完饭後的温泉芭蕾这样的甜点的时候，正好到了洗澡的时间。

◆


　是决定进入家族浴时间。
而且，进入温泉时，温泉必须接待二人。


　做完接待後，我们首先去了更衣室。
　正因为让家族使用的，所以不太宽阔。


「菲尔路先换衣服」

「嗯？」


　转向菲尔路的同时，她身上的衣服一瞬消失了。
　解除了包裹在身体上的魔力吧。


「……稍微隐藏下」

「因为是分身身体所以不害羞啊」


　不要说因为不是胖次就不害羞这样的说法。


　从菲尔路那转开视线，我脱去自己的衣服。
　在下半身上缠上毛巾，追上先跑的菲尔路。


　开门进入，是露天温泉。
　暗了的天空看起来变得清晰了，可以欣赏夜景了。


　菲尔路冲洗完身体，就急忙地进入了温泉中。
　我也在用热水冲洗完身体和头髮，去掉污垢之後，带着毛巾，泡在温泉中。


「……呼」


　泡温泉，是多久以前得事了？
　觉得身体的深处被暖和了。
　好像说热水对魔力的循环有很好的功效。


「相当好的东西啊」


　不知什麼时候，菲尔路来到後面。
　我不回头看，「是啊」地点头。
　凝视着夜景，沉默地泡在温泉中。


「稍微，意外」


　不久，菲尔路开口了。


「你也会做过激的事」

「……？啊啊」


　是昨天的事吗？
　是对马文他们做的报復的事吧。


「那样的事不是因为喜好。我只是还击我遭遇的事」


　绝望也好，被杀也是，全部都是我所遭遇的。
　因此，我发誓了，在背叛我的伙伴上，也做一样的事。
　使他们後悔。


「…………」

在背後，听见了菲尔路呼气。


「今天暂时就泡在温泉裡慢慢地来就好。不知道你自己注意到了吗，今天的你有着残酷的脸」


　和平时的菲尔路不同，说着像担心一样的和善的言词。
　这麼说来，她突然打算去温泉，是因为我吗？


「……菲尔路」


　打算说道谢的話，我把脸转向菲尔路的方向。
【译：前方高能！！！！】


「噢噢噢唏!?」


　看到背後的东西，我不禁發出了呼喊。
　说到原因，在热气腾腾的温泉水面上，菲尔路浮起着。


　——只是只有头颅。


　感到吃惊，不禁从那个场合急忙离开。


「为，为什麼。像妖怪似的」


　菲尔路的不满在眉毛上隐隐而现。
　然後那头颅在水面上“簌簌簌”地滑向我来。


「别，别靠近」

「唔……只是解除了分身身体而已，那个反应真意外」


　像闹别扭一样地那样说着，菲尔路那轻飘飘的头颅从水面上浮起了。
　在天空飞的头颅化的菲尔路直直地瞪着我。


　确实知道那傢伙只有头，不过，实际看到是第一次。
　马文的时候，也没怎麼看那傢伙的头颅。
　没想到是如此地让人震惊。


　由於“忽悠忽悠”地移动的头颅而不由得的後退的时候，有魔力从菲尔路的脖子放出。
　於是下一瞬间，头以下的部分被筑建起来。
　总之，赤裸的菲尔路呈现在眼前。


「所以说要遮掩啊!!」

「还不是因为你讨厌所以身体才出来！？」


　结果，没能对菲尔路说道谢。
　说真的，为是否真的担心我吗感到可疑。


　但是……确实能稍微地，减少疲劳。

