
“一群劣等杂鱼，郁闷啊 哟！”

聚集的冒险者们，被艾鲁托加的一击吹飞。
防御从远距离放出的火焰魔术，接近自己的斩击就用铁棒吹飞。
疲惫的他们，即使互相合作也跟不上艾鲁托加的动作。

“……那个怪物”

戰線不復存在，沒有我们的幫助米莎咂嘴。(戦線には加わらず、俺達の手当をしてくれたミーシャが舌打ちをする)
她的伙伴，看到那个样子，都咬着牙。

“艾魯菲……魔眼能使用吗？”
“沒辦法。维持分身体就拼盡全力了……”


最壞的状况。
吃下刚才的攻击，我的骨頭折斷了。
雖然把脫臼的肩膀搬回去了，但離完美的狀態仍相去甚远。

“伊织……這麼说来，你還有其他的藥水吗？”
“……那不行。不能用。拿着的份儿，已经用完了”


眼前，冒险者正被蹂躏着。

“可惡……”

艾魯托加是鬼族的精锐。
跟马文的手下是不同。
如果是万全的状态暂且不谈，现在的状态下战胜是不可能的。

“啊啊啊啊！”
“畜牲……！”
“不要过来啊……！”(引くんじゃねえ……!)


脱了铠甲的卓卢金猛烈攻击着，艾鲁托加把所有的攻击都弹开了。
就连可以一击打倒炎龙的攻击，也无法触及艾鲁托加。
在附近吃下铁棒的冒险者有幾个人在滚动着。
也许已经有人死了。

“不行啊，完全不行”
“什麼……！”


悠然得挥动铁棒，一边嗤笑的艾鲁托加。

“好！格！格格格格格格格格格！你和其他人不一样呀！”(有象ッ!　無象!　格!　格格格格格格格格格ゥ!　てめぇらとは格が違うんだよぉぉぉ!!)

艾鲁托加的铁棒，像暴风雨般挥舞着。
站在正面的卓卢金被吹倒，四周的冒险者们溃散，魔术师被铁棒發出的热风灼烧。
果然，对上他是没有胜算的。
等待的只有死亡。

“阿啊啊，逃跑啦?”

冒险者们被吹飞，艾鲁托加一点一点朝这边走来。

“这样啊！一群废物！拖延时间！自暴自弃啊！”

差不多，还有一分钟吗？。

“……伊织先生们请逃走吧”

锵的一声，米莎从鞘拔出剑。
与同伴们递眼色，

“会争取一点点的时间。在这期间，从迷宫中逃走吧”
“……你，想死吗？”


米莎忽然露出抽搐般的笑容。(ミーシャがふっと引き攣ったような笑みを浮かべる)

“治好了老爷子，给我修理商店的钱，伊织先生和艾路菲小姐。来到这裡，两人好幾次帮助我和伙伴们。
所以，在这裡，我会报答你们的恩情的。”

所以，那什麼啊。
因为你救了我，所以想把恩情还给你。
全部，都是背叛了我的人说的話不是吗。

“为什麼……那样的，要付出自己的生命，为什麼不抛下我们，然後逃跑，你们不觉得死亡很恐怖吗?”(这句我不太会，なんで……そんなんで、命を投げ出せるんだ。どうして俺達を切り捨てて、逃げようとしないんだ……」あんたらは死ぬのが怖くないのかよ……?)

“死是可怕的。但是”
“要说的話也是有恩情呢？既然有恩，怎麼能因为自己有生命危险就逃跑呢!?”(恩があるとでも言うのか?　どれだけの恩があったって、自分の命が危険なら逃げるんじゃないのか!?)


艾路菲默默地听着。
米莎的伙伴也沉默着。

“就是那样”

米莎笑着说。

“因为想帮助，就帮助了。就是那样”

“啊……”

那是。

成为英雄，陶醉在理想时的我说的台词。

“……走吧！”

带领同伴，米莎向艾鲁托加前进。
没有回头，只是笔直地向前。

“猫人种啊？鬼猫呀，地位不同，明不明白啊喂！”

敏捷的动作，米莎玩弄着艾鲁托加。
就这样连续斬击，她的攻击力不能给予艾鲁托加伤害。
同伴们的魔术，也打不中艾鲁托加。

怎麼办才好。

那样在风中，踌躇的时候。

“―――――”

视野出现噪音。
只是一瞬间，看见了谁的背影。
灰色头髮的男子。
外套翩翩飞舞，似乎有谁站在那裡。

视野马上就恢復正常了。

“ッ”(不知道这是不是状声词?)

想帮助你，所以来帮助你了。
这麼说来，之前的傢伙们，背叛了我。
被背叛的是我甜蜜的理想，因为没有看清本质。

想创造和平的世界，守护重要的人。
现在，我也觉得无聊。
想要拯救，从现实来看只是伪善。

但是。
现在，为了我们而战鬥的人………与见死不救逃跑的，是不同的吧。

“……好”

米莎拔掉并回收了翡翠太刀。
终於移动手臂，从口袋取出魔石。

“打算怎麼办，伊织”
“决定了。復仇的对象就在眼前。我一定要杀死他吧”


如果在这裡错过那个人渣，就不知道什麼时候能再见到他。
所以，要在这裡确实的杀了他。

“那麼，我就做我能做到的事”
“……不逃跑吗？”
“笨蛋。遭到铁棒毒打哦。怎麼能原谅他”


眯着眼睛，艾路菲这样说。

“而且，我被封印的时候，那个男的也在。和欧路特吉亚一起告知，你的部下已经死了，他们的遗骸是我部下的玩具。能够原谅吗？”
“……我明白了”


不逃避战鬥，决定了。


“噢噢噢噢噢噢噢噢噢噢！！”

艾鲁托加在声音裡加上魔力，發出咆哮。
响彻整个迷宫般的音量，米莎们从正面沐浴到。
同伴失去意识，吧嗒吧嗒地倒下去。

“——啊”

认识到的时候，美莎的身体正在倾斜。
全身失去力量、渐渐地靠近地面。
从影子看见自己倒下的身影。

“死吧，垃圾！”

铁棒挥了起来。

“————”

黑色长袍飘逸。
不知道谁介入了，挡住了艾鲁托加的铁棒。

“啊……”

应该逃跑的伊织就在那裡。

“杂鱼们的努力都白费了，阿马茨”
“……那又如何”


伊织和艾鲁托加对打。
果然，现在的伊织还是比不上艾鲁托加。

但是，却。

“英雄阿马茨——”

渐渐失去意识的米莎，
把伊织战鬥的身影，与传说中的英雄的身影重叠了。


“现在的我啊，和你是不同规格的哟！”

拼命地回避铁棒，与冒险者们保持距离。
对艾鲁托加来说，是看不见像垃圾般滚动的冒险者们吧。
跟他们保持距离，艾鲁托加似乎什麼也没有感觉到。

“没有那个女人的身影啊，逃了吗？。与此相比，不逃跑什麼的，阿马茨先生帅呐！仍自居英雄啊!?”
“咕……”
“那个英雄是现在的我啊！格格格格格叽啊阿啊阿啊！”
“格格吵死了……！”


“加速”，身体强化“魔技簒奪スペル ディバウア”使攻击的威力减弱，用“流水炮”和“坏魔”攻击。
攻击视野所见的目标，艾鲁托加焦急得往下挥舞铁棒。

“灼風フレイム?ブラスト”
“!?”


那个横扫的冲击波是足以将皮肤烧尽的热风。

“魔毀封杀”发动但没有阻碍地被突破了，全身被热风灼烧。
让人恶心的热和疼痛。
现在幾乎用完餘力了。

但是，我微笑，挑衅艾鲁托加。

“那麼说的話，对人类的歧视好象感到相当满足，是不是有很厲害的自卑感啊”
“啊啊……?”
“正因为这样，只记得一个不能大骂的笨蛋，格格 毫无疑问成为大叔了呢”(第二句我尽力了…，そんなんだから、馬鹿の一つ覚えにしか罵倒出来ない、格格おじさんになっちまうんだよ)

“啊啊啊啊……?”

对大叔这个称呼感到焦急呢，艾鲁托加把铁棒用力挥动。
没有针对任何目标，空有力量的一击。
用最後的力量去承受。

为了能够接下冲击，顺着势头向後飞去。
难看地在地面翻滚。
痛得不得了(痛みでどうにかなりそうだ)

“哈哈……大放厥词，结果是什麼也做不到”

但是，按照计划。

“啊……?”

下个瞬间，迷宫的温度下降了一个程度。
飘浮的魔素消失了。

“笨蛋 得救了”
“为什……么？”


艾鲁托加把视线转向房间裡面。

那视线的前方，迷宫核被得到了，艾鲁菲站在那裡了。

“为什麼……不是从房间裡消失了”
“视野狭小啊”


我拖延时间的时候，那傢伙在头颅的状态下被冒险者们隐藏着，朝迷宫核去了。
和冒险者保持距离，就是为了掩盖它。

“没有魔力的話就什麼也做不了。窃取迷宫核这件事好像已经發生了吧?”(魔力が無ければ何も出来ない、か。盗みを働くことくらいは出来たようだぞ?)

“你！”
“伊织 接住！”


艾路菲把迷宫核扔过来。
艾鲁托加移动了。
被那傢伙吹跑，是为了接住迷宫核。

但是。

“哈！ 我比你更快！”
“……！”


用铁棒摆好架势，艾鲁托加放出魔术。
可恶，破坏迷宫核，想烧死我吗……!

“下地狱吧，呜噢噢噢噢!!”

就在放出火焰之前。

“……啊！”

艾鲁托加的头轻轻地爆炸了。
有谁用魔术攻击艾鲁托加。

“这个垃圾……”

原来，在地上趴着的卓卢金，向艾鲁托加伸出手腕放出魔术。

就这样保持着表情，失去了意识。

“——”
“!”


就在艾鲁托加对卓卢金心烦意乱的那一瞬间。
在那间隙，接到了迷宫核。
握着虹色的迷宫核，吸取魔力。

即使吸收了魔力，也没有完全恢復力量。

但在这之前，能使用的魔力量增加。

这是赌博。

即使得到了迷宫核，也不一定能得到打倒艾鲁托加的力量。

体力也所剩无几，这是个很糟的赌注。

即便如此，我也赌赢了。

“可恶啊啊啊啊啊！”

艾鲁托加放出了火焰。

与至今为止包含的魔力量不同。
极大的火焰。

如果被包围，无疑是当场死亡。

正因为如此，才帮了我。

“————”

使用从迷宫核取回的全部魔力与大量的魔石瞬间发动一个魔术。

翡翠太刀被包在光中。
朝向猛烈的火焰，挥下太刀。

“――魔撃反射インパクト?ミラー”

将接受到的攻击，以加倍的威力返还对手的魔术。

在英雄时代使用的王牌。

消费庞大的魔力，即使是劣化了也不能使用的魔术。

如果是迷宫核提升魔力的这一瞬间，就可以使用——！

极大的火焰因为“魔击反射”威力上升了一倍。
然後，在那火焰的前方是艾鲁托加。

“没有失去魔力!? 为什麼……”

看着反射的火焰，艾鲁托加發出悲鸣。
不停地喊着绝对不可能。(声を裏返らせて、ありえないと連呼している)

“骗人，骗人！你和他们是，不，不同的……！”
“啊啊”


看着被反弹回来的火焰，艾鲁托加呆然嘟哝着。
我已经同意了。

“格 不一样的阿”

下个瞬间。

惊人的热量，把艾鲁托加包在裡面。

“呜咿啊啊啊啊”

就算是擅长火焰的炎鬼，也无法承受那样的火焰灼烧。

“好热，……好热阿！”

艾鲁托加的惨叫在迷宫裡回响。
就算那傢伙很顽强，也当场死亡了呢。
火焰消失，艾鲁托加回归大地了。(炎が消えるまで、ベルトガは地面でのたうち回った)

“啊……啊”

尽管如此，他是不会死的。(それでもなお、あいつは死なない)
濒死状态，回归大地。(瀕死の状態で、地面に転がっている)
挺适合你的。

――终於，復仇成功了。


