我睁开双眼，红髮的女性正在窥探着我的脸。

“!?”
“呀!”

我不由自主地挺起了身体，彼此的额头也因此互相碰撞了。
我因为痛而意识清醒了，意识到了自己在卡莲的宅邸裡。

“唉唉……”

………也就是，泪目的用手压着头的是卡莲。

从那以後，我把迷宫核，艾尔菲把自己的身体回收了。
我们把手持的回復药全部用完後，强行移动了身体，但到那就已经到达了极限。
我当场昏倒了。

“两个人还活着，真的太好了……!”

卡莲摇晃着红髮，用颤抖的声音说道。
据卡莲所说，卡莲率领的士兵把我们送到了这裡。
迷宫停止後，陷入恐慌状态的魔物逃到了迷宫之外。
正在等待它们的花莲们全部消灭它们後，又踏入了迷宫中。

内部的毒沼由於已经失去了迷宫核，好像是无害的。
其他的陷阱也没有动。
在迷宫停留了半天，将我们救了出来。
在那之後，我好像睡了俩天以上。
於是，我想起了那傢伙的事情。

“……是这样啊。艾尔菲呢……”

房间裡没有艾尔菲的身影。
那个傢伙，比我还要受到更多迪奥尼斯的虐待。
喝了回復药后，她和我一起向迪奥尼斯报仇……
难道说？我这麼想。

“艾尔菲桑的話，她和伊织一样昏倒了。”
“……那麼，现在呢?”
“她在半日时前醒来，现在正在吃饭。”

……啊啊，很有精神呢。

“村子裡，没事吗?”
“是的，现在平息下来了。”

但是，在迪奥尼斯的袭击中，似乎出现了幾名牺牲者。
卡莲说，这两个领地被毁坏了，现在正在进行着復兴工作。

“……是这样吗?”
“请不要露出那样的表情，这种程度的损失，多亏了伊织桑和艾尔菲桑的功劳。”

我看到卡莲的脸，虽然有疲惫，但也没有像以前一样的绝望。

“多亏了你们两个人，我的领地……不，很多人得到了拯救，而且还帮我们清除了威胁着帝国的迷宫……真的，非常感谢。”

面对低下头的卡莲，我语塞了。
我只是，为了报仇而行动罢了。
没有被行礼和说到这种程度的权利。

“……对了。这个，是我从水魔将那取回来的。”
“‘要石’……！”

在杀死迪奥尼斯前，我好好地先把这个回收了。
卡莲用手接收拿回了要石，垂下了眼睛。

卡莲点着头，说：“这个石头是‘想守护百姓’的我们莱福家人留在世间的意志的结晶。谢谢。”
“是这样吗?”
“但是……这样啊。”

卡莲抱着石头，低声说道。

“已经，可以不用封印迷宫了。”

卡莲一下子失去幹劲的跪在了地上。

“这样啊……这样啊……”

泼咯泼咯地，泪水从她的眼中滴落下来。
父母死亡，她作为领主必须尽到责任。
我听了一段时间，从那个重担中解放出来的一名女人的呜咽。
——————————————————————————————————

火焰發出。
排队的牺牲者的尸骸瞬间燃烧起来。
我看见黑烟升到空中。
因迪奥尼斯和手下的魔物而死亡的人们。
而且，被迪奥尼斯所制成标本的女性们正在被吊唁。

“……”

被结晶束缚的人们已经死亡了。
我和艾尔菲，也想不出帮助她们的办法。
她们被关了多少年了呢?。
从迪奥尼斯的口气听来，她们再也没有家人、朋友、住过的村庄了。
在得到帮助的场合，难道不是只会让她们更加痛苦吗?。

“……不。”

这是任性的生者的理由。
谁也，都不想死。
应该想要得到帮助。

迪奥尼斯说因为她们救了我，所以被他杀了。
被杀的人，是否会恨我呢?。
我不知道。

但是我只能祈祷着她们能够安眠。

“……对不起。”

突然，背後传来了声音。
站在那裡的是，陌生女性和少女。

“大人，是您把我的母亲从水魔将解放出来的吧?”
“……”

被制成标本的其中一位女性，是一名住在这个村子裡的人的母亲。
据说，眼前的女性，在与魔王军的联合袭击之前，离开了村庄。
所以，只有她一个人得救了。

“拯救了母亲……非常感谢您。”

女性低下了头。

“……”

在女性身边的少女，抓住我的衣摆，说道。

“感谢对奶奶的帮助!”

蓝色的天空中，有黑色的烟升起。
遥远、而高。
————————————————————————————

在卡莲的房子裡醒来後，过了三天。
在此期间，卡莲找到了从迪奥尼斯所获得的情报的人物。

“如果是那个人的話，现在在教国经营孤儿院。”
“……孤儿院，是吗?”
“是的，地图上是……这附近吗?”

卡莲的指着的地方，有着我下一个復仇的对象。
而且是，两个人。
好像是作为夫妇，在那裡生活着的样子。

“那两个人，以前都是很优秀的鍊金术士。听说现在他们像圣父母一样养育着孤儿。他们是伊织先生你们认识的熟人吗?”

圣父母，呢。

“……嗯。就是那样。”

啊啊，是三十年来的熟人啊。

“原来如此……但是，这一带是有奇怪的传闻的地方，请小心。”
“奇怪的传闻?”

说到底只是个传闻而已，叮嘱一样，卡莲说。

“在这附近有幾次——“英雄阿玛茨”被目击到。”

那是完全出乎意料的言论。

“……”
“……哦。”

不过假如是这两个人的話發生什麼都不要紧吧，卡莲苦笑道。
__________________________________________

离开了雷福德领地，我们朝着东边走了过去。

“我们被招呼去帝都，不去好吗?”

幾天前，皇帝陛下的使者到达了雷福德领地。
对於讨伐迷宫的我们，他想要见我们一面。

“感谢只是有名无实的，去了的話什麼都会被问到的。去了的話只会浪费时间。”

所以，我们郑重地拒绝了那个。
托皇帝方的人十分明理的福，所以我们没有被强行挽留，因此我们正朝着下一个目的地前进。

“嗯……唔。”

在旁边，我听到艾尔菲的咀嚼的声音。
艾尔菲哈克哈克地吃着的是，将烤的鱿鱼，章鱼或是贝类塗上酱汁串好的烤串。从刚才开始她就吃了好幾串，手和嘴都变得啪嗒啪嗒了。

……好脏。

“……对了。在迷宫裡，取回了什麼部位？”
“俩脚”砰砰地敲打着膝盖，艾尔菲回答道。

“那麼，就是说你的头，双手双脚都取回了吗？”
“嗯。剩下的只有躯幹和心臟。那样的話，伊织怎麼样了啊？”

艾尔菲所说的，是有关我的魔力的事吧。我已经吸收3枚迷宫核了。

“………老实说，离原来状态的程度还挺远的”

比起以前，我进一步能使用的魔力增加了。
大概有全盛期的4成实力左右吧。
和完全没有魔力的时候相比，已经相当厲害了。

“……嗯。啊，对了。那麼，那个心象魔法是什麼？”
“那个吗？”

“英雄再现”。

那时，自然而然地浮现在脑海中的心象。
因为是一瞬间的事，所以我不怎麼记得详细的事……。

“大概……是将在英雄时代那时的我的力量再现的魔法……是吧?”

我觉得，自己只是随波逐流而行动着而已。
但是，多亏了艾尔菲的話，我才知道并不是这样。
我是抱着“想帮助他人”的心情，在行动着的。
………根據那结果而得到的心象魔法，是将那英雄再现出来的技巧虽说如此也稍微有点複杂。

我已经，不想成为勇者也不想成为英雄了。

“现在能使用吗？”
“……很微妙。并不是用不了，但是没有那时的感觉。”
“这样啊。嘛………不管怎麼样，多亏了你的心象才得救了。谢了，伊织。”

停下脚步，艾尔菲注视着我。
正面接受那份注视，我开了口。

“……要说谢谢的，是我这边。”

艾尔菲被迪奥尼斯劝诱时，我以为我会被背叛。
再次，被同伴背叛而被杀害。
但是艾尔菲对我说了。
如果要背叛同伴的話，死的那边还比较好。

而且，之所以我能使出心象魔法，是因为有了艾尔菲。
如果没有那句話的話，我会就那样绝望了，把全部都放弃。

所以说，要说谢谢的是我这边。

“多亏了艾尔菲，我没有放弃。我不仅仅是随波逐流地行动着，这样的想法涌现了出来。所以说，谢谢你。”

”……哼、哼嗯。当然了。”

转过身来，艾尔菲自大交叉起胳膊。

“之前说过了。能不能相信我，你自己决定。”
“……嗯。”

迪奥尼斯说。
復仇者们间习惯在一起很奇怪。

我也是，这麼想的。
虽说目的相同，但结果仅仅只是被背叛者们，在一起互相舔舐着伤口而已。

尽管如此。

“和你在一起的話，我想，最终能完成復仇。”

因此。

“——我相信你，艾尔菲斯扎库。”

迟了一会，艾尔菲笑了。
但是那张脸被海鲜串的料汁弄髒，各种意义上的浪费。
尽管如此，我完成了对迪奥尼斯復仇，我觉得又再往前走了一步。

——因此。
奥利维亚，迪奥尼斯。
目前为止，对四人復仇了。（译：？原文就是只列举2人的名字）

但是，还有幾个另外的復仇对象。

如果能使用入手的心象魔法的話，也就能突破琉泽苏的“因果返葬”的吧？

等着吧。
将剩下的復仇对象，一个不留地杀死。

——为了，我能向前迈进。



