日上中天
东大陆上乾燥的风抚摸着脸颊。
生长着稀疏的短草，这样荒凉原野的地平线，在视野的正面，一直延伸到了尽头。
有时能看见左右出现了山啊，森林啊，湖啊等自然风光，不过，完全把那视作一行人路上的障碍那样，那个马群并没有停留。
那是被时代的奔流卷入的魔王一行。
把逆流中一点点的顺风紧紧抱住，他们沿着通往雷缪泽的道路前进了。
那样的他们和『某个存在』邂逅了，那是从內烏斯=高斯公国出發，接着迎来第一个早晨的事了。
早晨的前面有拂晓，拂晓之前有夜晚。
在邂逅拂晓之前，他们首先迎来了夜晚。

◆◆◆

最初的夜晚在水边的附近度过。
缓缓流动的河川的中间，在积存了小小的湖的旁边。
那个充分吸收了水分的地带显得相当肥沃，形成了丰富的植被。
在沙漠中，确实是绿洲的身体吧？。

是在古代开始的每一天，就渗入了旅行商人血汗奔走的行商街道，有些距离的地方。
从没多少人踏入的痕迹看来，不是完全野生的地方，从正常的路线来说不太远，又不算太近的路线，可以说是自然的程度，对想躲藏的魔王们来看，是很方便的地方。

 由幾个人交替担任岗哨，各自睡眠的同时把旅途奔走中积累的疲劳淨化了。
“梅雷亚，好好清醒着吗？”
“清醒哦。看守的人睡着了要怎麼办啊，萨鲁曼。”
在这种状态下，除了贪图睡眠以外的魔王们在稍微离开的地方，有一个小灯火和眼睛睁开的人物。
是两个人。
一个人在大树的旁边上，背靠坐着，另一个人向那裡慢慢地走近。
——是梅雷亚和萨鲁曼。
“开玩笑的。在这种时候哟，最初应该怎样打招呼？还是会犹豫的啊。”
灯火——火一点点地烧着。
顺便说一下，不是普通的火。
《生命之火》，《生命之炎》。
也就是说，那个〈炎帝〉莉莉乌姆做出来的不可思议的火炎。
把梅雷亚的脸照亮的灯火，比拳头小一点尺寸的火块，火源也没有的，在梅雷亚脚下孤零零地蹲着。

经<炎帝>之手生出的寄宿着生命的火炎，实在是不可思议，比什麼都便利。
以她的固有术素为燃料，不加柴火也能持续燃烧。
并且在感知到四周出现气息的时候，自己的身体就像隐藏到附近的水边一样投河了。
摇摇晃晃地摇曳的火焰长着纤细的手足，站不住脚般快步跑到水边，只有一次犹豫的那样把身体拉开，但终究自尽了。嘶嘶，响起心情畅快的声音同时，生命之炎熄灭了。
那个时候，梅雷亚脚下的小生命之炎，对萨鲁曼靠近梅雷亚起了反应，那虚幻的生命结束了。
“啊，那个，总觉得有点难过。还有，实在对不起。”
“住口，别说了……眼泪要出来了。”
为了这样偶然的瞬间，想不到生命之炎会投河的人，所以在树丛下面隐藏着挖好的洞裡，预备好的生命之炎们在那狭窄的地方待命，一体消失的话，就会有一体从那裡出来。
殷切等待自己出场机会的身影总觉得好玩，另一方面会让看的人觉得悲哀。
“不是水边，而是回到洞裡就好了……”
“似乎是有就近原则的……。在体内的术素残量减少了的状态下，最後决定选择华丽的散落方式。”
“武士吗！？”
萨鲁曼用手指在眉间捏住，那压抑头痛般的举止。
顺便一提，莉莉乌姆本人看到那个跳进水中的小小的生命之炎。
“那傢伙，笑得好厲害啊！”
“莉莉乌姆看上去就像恶魔！”
捧腹笑着。
明白是她自己制作的，也知道她是对生命之炎下了那样的命令。
那个命令的正确性也明白。
但是，看到生命之炎把自己的生命扔似的往河裡自杀的样子，莉莉乌姆愉快地笑着的时候，梅雷亚和萨鲁曼在心裡害怕极了。
“我想，我是不会有反抗莉莉乌姆的想法”
“大概从年幼的时候开始，就和生命之炎在一起成长起来的莉莉乌姆，并不是什麼不可思议的事情……。倒不如说，是好好的利用自己的术式进行娱乐的东西……吗”
於是，在梅雷亚半笑着回答了的时候，萨鲁曼往那个旁边走近，以相同的姿势坐下了。
梅雷亚在迎接萨鲁曼同时，突然发现很好的香气掠过了鼻腔。
是什麼的香味呢？再次把目光移向萨鲁曼，在萨鲁曼手中握着两个金制的杯子。
香味儿就是从那杯子裡升起。
“看啊，这个金杯子，是最高的爱好吧。”
“这麼好的气味也掩盖不了那暴发户的臭味，‘杠杠’的飘过来了”
萨鲁曼一边说一边递给了梅雷亚杯子，梅雷亚苦笑着回答的同时收下了杯子。
在夜幕下再加上生命之炎模糊的灯火，反射到了那个金色的杯子上。
像是在旅途中的一杯茶，这种虚幻感觉的气氛也有了，不过，在脚下开始用手脚做屈伸运动的生命之炎的原因，气氛减半了。

梅雷亚往在萨鲁曼那收到的杯子中望去，究竟是什麼在裡面那样，弯下腰窥视着。
“甜的气味和酸的气味”
“啊，柠檬汁味的，卡密拉？这麼叫的。特别是我长期居住的街的饮料。街道的特产品卡密拉酒，用常见的柠檬汁——嘛——调成多酸味的鸡尾酒，最後再加一些佐料就成了。”
“哼哼”
“卡密拉酒就和名字一样，是使用了卡密拉水果果子制成的酒。可那个有点太甜了，本来作为礼物打开一本就已经很好了，但对於常用的当地人类来说，到底是很执拗的。所以，这样和各种酸味混合起来的话，会配出各种不同的味道。”
“唉，真熟悉嘞”
“都说是长期住的地方。所以，在内乌斯=高斯寻找东西的时候看见了，用剩餘的钱买了。不要对金钱的亡者说哦。”
萨鲁曼砂色的头髮在晚风中飘舞笑着，随後把自己金杯子裡的卡密拉？柠檬汁一口含进嘴裡，然後慢慢咽下。
梅雷亚斜视看着那个，下定决心後自己也把金杯子移向口中。
“甘——！酸啊！”
“哈哈哈哈哈哈！你喝的是酸味很强烈的！”
“你也和莉莉乌姆一样没有区别啊！”
“是疏忽大意的人不好哦，这就像是金钱的亡者的话啊。好像不曾说过，所以算了。”
萨鲁曼表演出诙谐逗人的样子，拍了拍梅雷亚的肩膀。
梅雷亚被酸得缩拢着口，但注意到卡密拉？柠檬汁其不可思议引人上瘾般美味的事，再次重新把它含住。
甘甜中带有酸味。
甜味让身体的疲劳缓解淨化，酸味使其身体到各个角落为止，遍布那个热气。
温暖的呼吸自然泄漏。
“怎麼了，习惯了就会觉得很棒吧？”
“我不否定”
“真不坦率啊”
萨鲁曼又笑了。
“其他的人呢？”
“还在熟睡呢。大家都累了吧”
萨鲁曼用下巴指向树木林立的深处所在。
由於地形和植物的原因，无法纵览全貌，不过，好象在那个对面有其他的魔王们正熟睡着。
静心倾听是可能听到昆虫凉爽的合唱中混进一个左右的寝息。
然而梅雷亚是相信萨鲁曼的，没想过硬要去窺看。
作为代替，对这样告诉自己的萨鲁曼说。
“萨鲁曼呢？”
“睡得很充分了。多亏你为我站岗。所以，换人。你也去睡吧！”
“我不睡觉也没关係哦。我现在就是这样的气氛吧！”
“看上去好像是呢。看著，深切地这样觉得。”
梅雷亚再次强调的话，萨鲁曼苦笑，并没有否定。
“但也不是完全不用睡觉吧？”
“嘛——嗯。到底是连续数日了，到底是累了。”
“那现在就把那个可持续天数重新復原吧。目前还没有发现追兵的迹象，所以不要紧。”
 梅雷亚认真的观察萨鲁曼，从他的表情窥视着是否真的休息够了。
如果发现有点累的话，就这样自己继续望风。
但是，萨鲁曼就一副萨鲁曼的样子没有丝毫的迹象。
如果按照当初的预定，就应该老实地交换吧。
结果梅雷亚在萨鲁曼的脸上看不出丝毫疲劳的颜色，所以决定遵从他的话。
“我明白了。那麼，我也稍微去睡一会吧。”
“啊，就这麼办”
“那麼，这个。”
 於是，梅雷亚一手拿着把不知不觉完全喝乾了的金杯子，和另一只手拿着的小小的树枝一起递向了萨鲁曼。
顶端那头有像插着丸子一样的树木叶子被插着，不可思议形状的树枝。
“和那生命之炎遊戏的话能忘记时间。如果把刚才的叶子，团子烧了的话就算你输。”
“你是能想出不可思议的遊戏的天才啊！”
“玩得太过分，只顾着这一点，望风就会变得疏忽哦。”
梅雷亚模仿起刚才的萨鲁曼，自己也以夏羽那商人的气氛，一边模仿做作的表演。
“你是那样的吗？——哦，不过这也相当好玩啊。哦，这边——看啊，意外的难缠。”
 萨鲁曼一边把卡密拉？柠檬汁注入了的金杯子，另一只手握住的梅雷亚特制的枝条，立刻与脚下的生命之炎分个高低了。
叶子，团子的尖端在生命之炎附近转动的话，生命之炎就会为了燃掉叶子，手足并用地扑过来。
这跟用狗尾草逗弄猫相似呢。
 “……但是，这傢伙也会把身体投向水裡。”
“睡觉前别让我有难过的心情啊”
“啊，真是抱歉”
萨鲁曼露出得意的脸笑了笑。那一瞬间树叶被生命之炎燃烧。
梅雷亚看到当时的情形，最後展现出快乐的笑容。
“那麼，晚安”
“好，做个好梦吧”
朝着大家正立着鼾声的树丛深处走去。
萨鲁曼把新的叶子在树枝的尖端扎好，一边目送梅雷亚的後背。
但是，梅雷亚的个子变得快看不见的时候，萨鲁曼突然停止了手的动作，浮起了沉思的表情。
“……”
萨鲁曼在梅雷亚的那个背上，仿佛看见了不可思议的虚幻。
在战场上表现出如此压倒性的存在感，不知道为什麼他的後背就这样，能随时地融入了空气之中。
那个理由理解不能。
但是，对萨鲁曼来说，那是非常令人不安的。
终於梅雷亚的个子是从视野中彻底消失，土地踏出的脚步声也听不见了。
只剩下草丛裡听到的感觉清爽的虫鸣声，以及从刚才开始在脚下听到的生命之炎在噼噼啪啪的声音，在那个场合某个大发雷霆的声音开始了。
◆◆◆
夜幕慢慢上升，然後清晨。
在晨曦的香味飘浮的时刻。

一个少女比谁都早醒了，为了不吵醒其他魔王们安静地站起来。
那是一路到达内乌斯=高斯公国为止，运用其特异的术式独自支撑的少女——

红髮的〈炎帝〉莉莉乌姆。

她沐浴在树木的缝隙裡，清晨的光芒当中，一个人安静的步向水边。

◆◆◆

“萨鲁曼？”
“啊，你是第一个哦”
在水边附近屹立着一棵格外大的大树旁边。
坐着砂色头髮的男人。
是拥有相当端正眉目的青年。
“嘛，说不定对眼睛稍微有点不好。”
“从早上开始就说我草（卧槽）。”


（原文「朝からひでえ言い草だな」 这算不算直译？）


“没关係，有在心裡夸赞”
“在嘴上也说说啊……”
一边相互说起俏皮话，一边互相保持着距离。
虽说如此，从上一个内乌斯=高斯公国出来後，有感觉魔王之间的距离拉近了的心情。
莉莉乌姆厌烦了一样把话还给萨鲁曼，然後发现他手裡握有不可思议形状的树枝。
“什麼，那个”
“啊？——啊，这是梅雷亚做的。为了逗你的生命之炎的道具。”
这麼说完萨鲁曼随即以脚下的生命之炎为对手当场表演了遊戏的方法。
萨鲁曼也相当熟悉生命之炎的习惯了，彼此持续攻防的拉锯。
“哼，我不太明白，不过，『那傢伙』的燃料快耗尽了，要消失了。”
“啊！真的吗？！这傢伙是我的毕生的对手啊!?其他幾个傢伙都把身体冲水里去了，剩下就这样消失的傢伙也有，经歷很多辛苦了啊!?这傢伙是跟我力量是同等级的，休息中夹着幾次拳脚相交！这傢伙已经是我朋友了！居然要消失什麼的――！”
“啊，消失了。”
“啊！！”
嘶~
比跳入水裡消失的时候还要轻的声音，萨鲁曼脚下了生命之炎消失了。
树木间隙中流入的风在吹动，以为只是在摇曳，没想到消失了。
之後，莉莉乌姆发现了萨鲁曼眼角漂浮着泪水。
“嗯，等等，什麼，有点恐怖的说”
像是故意的一样的说给你听。
“你，你的！那傢伙还活着……！”
“啊……啊……嘛，我不知道那是什麼感觉，不过，会习惯的”
一瞬间，萨鲁曼看着莉莉乌姆的脸绝句了。
会习惯的，说出那话瞬间的莉莉乌姆的脸，尽量抑制住感情一样的无表情。
萨鲁曼看到那个醒悟了。
“是这样吗？……你其实也伤心过的啊……。你也有生命之炎消失时，会变得难过的少女时代——”
“现在只是觉得有趣而已”
“果然是恶魔啊！”
“知道了，赶紧回到那边吧。看来大家要做好準备之前，还稍微有点时间，不去小睡一会？”
莉莉乌姆向萨鲁曼伸出手，用力把萨拉曼的身体拉起来後，嘶嘶，赶走一样地挥了挥手。
“我来替换岗哨的，你啊，快去”
“要和生命之炎离别了！”
扔下那样的台词，萨鲁曼往大家的身边走去。
在洞穴被剩下的生命之炎，凝视着那些生命之炎们，
“你们辛苦了，下次再见。”
虽然很小的说，在它们燃料耗尽在天空中消失的样子，一直看到了最後。

◆◆◆

莉莉乌姆在萨鲁曼返回了以後，一个人在附近的水边轻轻的洗脸，想顺便把头发上的脏东西洗掉。
果然洗澡是不行的，暂且是最低限度。
因为水本身很清澈，所以有想跳水的慾望。
“要是途中谁来了”
即使来的是同伴，如果是男人就最糟糕了。
“啊，但万一，来的是梅雷亚的话就没关係的吧”
没关係的意思绝对不是等同可以看。
只是，相比其他的男人，总觉得梅雷亚作为男人有点空气稀薄的感觉，所以才这样想。
万一被他看到，说不定不会动摇的这一点上，没关係的意思。
“嘛，不是赞美的词啊”
在本人面前就别说了。这样想到的。
莉莉乌姆暂时就这样用水洗落头髮上的脏物，
因为头髮很长，所以面朝着水的话，会溅湿衣服。因此，把脸和视线都平放在水边，一边侧着一边洗头发。
然後才抬起脸，
“呼”
一边吐出呼吸一边把视线放回正面，
“我去”
巨大的黑块映入眼帘。
“——”
莉莉乌姆禁不住快要喊出叫声了。
不知道什麼时候接近的，在水边的另一边。

有一条黑色的龙。
