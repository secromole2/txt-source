誒哆，叔叔，您刚刚说了什麼？
伊丽丝歪着头，（那如丝般）柔顺的银髮横着垂落了下来。
鲁鲁这样想过，如果可以的話这样的事情是不想告诉（她）的，（但）那样是不行的。
如果要在这个世界，这个时代生存下去（的話），以上（所说的）那件事必须得接受，（这是）最低限度。
所以直截了当地跟伊丽丝说了。
“伊丽丝，从那时开始的现在，从我被勇者杀了的那个时候起，已经有数千年的岁月流逝了。姑且说在前头，这绝不是误会。确确实实地进行了确认，是事实。虽然話是这麼说，也只是从文献和传说中推测的而已，也不是没有误差的可能。”
鲁鲁一边这麼说着一边暗示着搞错的可能性也是有一点点的。
那是对伊丽丝的关心。
我认为用这样的方式说的話就比较容易让人接受。


鲁鲁无论如何是不会搞错的，这个时代已经是从那个时候起经过了数千年（的事情）确信了，这种确信，要让眼前的这个少女马上接受这样的事是不可能的。
暂且，（先）作为可能性埋入脑海裡，然後慢慢地理解，（慢慢地）增强到确信，鲁鲁是这样想的。
但是，鲁鲁稍微有点把伊丽丝给小视了。
虽然伊丽丝被鲁鲁的話所惊讶到了但毫无疑问地记住了，尽管如此没有说那个说辞绝对地接受不了。
才不是那样。倒不如说明确地在自己心中（好好地）揣摩，在此基础上提出了疑问。
“那是……什麼，那样的东西（肯定）能确信的吧，叔叔已经确认过了的吧？”

这让慢慢地点着头的鲁鲁惊讶到了。
伊丽丝好好地选择了措辞，即使拥有聪慧的头脑，但是尽管如此还是个小孩子而已。
但是与鲁鲁不同，真真正正的，毫无疑问的是十岁左右的考虑。
虽说是在鲁鲁的死後生活了数年的时光，即使如此毫无疑问地没有成年。
然而，伊丽丝思考着，冷静地对鲁鲁说道。
“叔叔，叔叔，我，没问题的说……不能说完全没有做好觉悟，但是，亲人不知什麼时候就去了遥远的地方这样的事，很伤心，但稍稍习惯了点所以……”

对於那句話，深深感到了惋惜的念头，一次又一次地感受到被撕碎了的悲伤所残留下来的渣滓。
鲁鲁如此感受到。
无论是伊丽丝活着的这个时代也好，还是鲁鲁活着的那个时代的也好，是一样的。
人的生命，简直是难以置信的脆弱，同胞们在离自己仅仅幾十公分的身旁所凋零的身姿，两人都已经看过无数次了。
承受过那样的经歷，无论如何对还很年幼的伊丽丝而言，询问觉悟的所在显然是太过於愚蠢的行为。
“……对不起呐。”

所以，鲁鲁为此道歉。
那是，作为魔王而没有保护好种族，而且，作为她的熟人而没有保护好自己的性命的事而道歉。
伊丽丝用那样直率的眼神接受了，然後绽放出温柔的微笑。
“没关係的哟，叔叔，我认为叔叔已经最大限度地努力做了。如果不是叔叔的話，恐怕魔族们都活不到那个时候，作为旗帜也好，作为值得骄傲的王也好，都是我们魔族的希望。因此，没关係的哟。对不起，（眼睛）湿润了。承蒙您的关心，现在，比起那个，把握现状才是必要的。对麼？叔叔。”
於是乎，伊丽丝立马振作了起来。
鲁鲁知道了面前这个女孩（所拥有）的坚强。
接受全部（的事情後）而（继续）前进（这样的）气量之大，觉得（十分地）感动。

或许，那正是因为是伊莉丝也说不定。

但是鲁鲁没再深入思考下去，现在只是继续把必须告诉给伊莉丝的东西告诉给她而已。

「是啊……叙旧什麼时候都能做呢。那麼……伊莉丝，让我们重新确认下吧。现在的时代，已经从那个时代起过了数千年的岁月，并且还有件重要的事情是现在好像没有像我或伊莉丝那样的魔族这个种族。」

「……也就是（说），因为在这数千年之间，被灭亡了，吗？」

伊莉丝看起来很悲伤似地低下视线（發出）微弱的声音询问了。
魔族的灭亡，这是曾幾何时在鲁鲁的脑中也（思考）过的悲伤事实。
但是现在伊莉丝在这儿，也许有同样倖存至今的魔族也说不定。
所以鲁鲁摇了摇头，（因为还）不能捨弃掉（这份）希望。

「不（いや），那个不清楚。不明白的事情太多了……像我们这样的魔族，在现在的时代被称为『古代魔族』，被看待成神話中的登场人物。有没有都不清楚……梦想与浪漫的象征那样的，呐（な）。」

听着鲁鲁的说明，伊莉丝的（脸上）露出了难以形容得奇妙的表情。
我能理所当然地理解（她）即使得知自己们的种族变成了神話中的存在也（只能）感到迷茫的心情。

「这也就是说……（我）成为了难以名状的存在呢（わ）。但是，我就像这样存在着……如果被发现的話会被捕捉吗？」（伊莉丝说話时有各种敬语和语癖）

伊莉丝稍稍担心地说道。
但是，鲁鲁对这个问题摇了摇头。

「我想大概不必过於担心，伊莉丝吹飞的那个冒险者……叫古蓝（Gran）什麼的，虽然看到了沉睡中的伊莉丝，但是并不知道（你）是古代魔族。」

根據这个事实，鲁鲁推测伊莉丝恐怕被认为是人族（Human）了。
连分辨种族的道具都是否存在的现今，这样的技术力正在下降。
因此，鲁鲁认为能很容易地找到漏洞/解决方法。
但是伊莉丝比想象中还慎重地追加了更细的问题。

「如果只是那一位不清楚罢了，呢？」

确实存在着那种可能性。
本来的話应该在大城市中，在更多人面前确认的，但是现在很难去实行。
但是，古蓝以及优蜜丝（Yumisu）这样的冒险者毕竟基本上是万事屋。
他们的知识（涉及）从常识性的地方到专业性的东西之类的各种各样。
至少常识性级别的东西是不可能不知道的，因此即使是尤蜜丝也不可能会知道。

（因为）尤蜜丝似乎是古代魔族控（フリーク=freak=发烧友，嗯~···翻成控是故意的）的样子，（所以）也可能了解的更详细。即便如此如果不清楚古代魔族的特徵的話，鲁鲁的推测就是正确的吧。
因此鲁鲁把这些想法告诉了伊莉丝。

「与其说古蓝，不如说是（因为）没有如常地传承下来。由於那傢伙似乎是个相当有本领的冒险者，如果那傢伙不知道的話，我认为肯定不会被人一下子认出的。学者之类的不清楚……这个之後再确认吧。」

说明至此，伊莉丝终於看起来接受了的样子。

（她）笑着点了点头，然後如此说道。

「我明白了。单从对話来看，只要我呆在叔父大人（おじさま）的身边的話，就有种没什麼障碍的感觉呢。」

伊莉丝说的話碰触到了核心。
是啊（そう），到这裡为止，或许能说没有问题吧。
种族的问题在某种意义上而言是总会有办法的。
但是，伊莉丝要呆在鲁鲁身边的話有必须要克服的问题。

「是这样呢，基本上没问题。现在（你要）照我说的做，伊莉丝要坚称（自己的）种族是人族，是个没有父母的孤儿……但是刚才我也说过，我有父母。不克服这个问题的話就不能住在一起了。」

虽然这样说，但是鲁鲁觉得好像也没什麼大问题。
既然伊莉丝已经在一定程度上了解了这个时代的事情了，之後再完成细微设定的调整的話，总会有办法的。
伊莉丝听了鲁鲁说的話後，歪了歪头/感到疑惑了。

「是关於父母，吗？……是这样呢，我什麼技能也没有，只是作为食客给你们添麻烦呢……」

对如此说完後zun（ずーん）地开始消沉起来的伊莉丝鲁鲁慌忙地摇了摇头。

「不不（いやいや），不是这样啊。不是那样……那样（感到）为难的事情。只是，突然带女孩子回家说想一起生活，（父母）会说，是这样啊，（什麼的）是不可能的吧？因此，在考虑该怎麼办呐……」

於是伊莉丝摆出，什麼啊，是这种事情吗。的表情pon（ぽん）地拍了下手，像提出好主意似地提议道。

「那样的話，就以作为女仆（的理由）来雇佣我哟！」

这是个出人意料的提案。
但是，毕竟也不是不可能。
贵族的房子裡也有佣人。所以，也不是办不到。但是，伊莉丝是好友的女兒，被相当地爱护的事情是知道的。
因此，在鲁鲁的心中不由自主地想纵容她的心情也不是没有。
所以，鲁鲁摇了摇头。

「这是不行的……手会变粗糙的。」

因此，作出连能否成为理由都（有点）微妙的说法是没有办法的。
但是并没能说服伊莉丝的样子。

「不要紧的说（です）！我即使这样/别看我这样（我）也有作为魔族的末席的立场的说哟。因为洗刷工作那样的（工作）而使手变粗糙是不可能发生的哟！」

确实如此，但是鲁鲁想说的并不是这种事。
然而，（因为）实在没有表达方式，（因此）没有办法的鲁鲁决定推迟这个问题了。

「……嘛啊（まぁ），那个（暂且）保留吧，之後再决定就好。嗯（あぁ），一定是那样。不管怎样……（先）一起返回现在我住的村子，与母亲见一面吧。然後，无论如何，我都会说好話让你被接受的。嗯（あぁ），就那样做。」

（看着）最终开始研究起漫无计划的作战的鲁鲁，伊莉丝叹了一口气，

「……所以，明明（当）佣人就好的说……」

地这样说道。

◆◇◆◇◆

然後，过了一会儿後古蓝醒来了。

「……好痛呐……哎（おい）？」

在被伊莉丝吹飞的时候撞到了吧，（他）一边揉着脑袋与腰部，一边起身，然後在睁开眼睛确认了伊莉丝的瞬间。

「……tsu（ッ）！？」

zuza（ずざっ）得尽量地後退，捡起掉在地上的大剑摆出了毫无大意的架势。
但是，伊莉丝一点袭击过来的样子也没有，而且坐在她身边的鲁鲁正大大的歪着头，（於是他）从远处大声喊道。

「……喂（おい）！这是，怎麼一回事！」

「啊（おぉ），（她）似乎误会了哦！不会再袭击过来了哟！」

鲁鲁也不甘示弱地大声喊道。
於是古蓝，

「啊（はぁ）！？　……不明白意思哟……可是，似乎真的已经没有会袭击过来的感觉了呐，没有杀气……」

（他）一边自言自语地嘟囔着一边靠近了过来。
大剑也收进剑鞘背在了背後。
感觉似乎已经没有战鬥的必要了。

实际上，因为伊莉丝最初表现出来的敌意已经毫无踪影了，只是nikoniko/笑眯眯（ニコニコ）地看起来心情不错似得微笑着，（所以）古蓝也陷入了惊慌失措的气氛中了吧。
但是，似乎并没有忘记初击的强度的样子，（他）用看着野兽似的目光盯着伊莉丝，然後询问了。

「……喂/呐啊（なぁ），小妹妹。」

「是（はい）。有什麼事情吗？古蓝大人。」

「虽然没有（被称作）古蓝大人的资格……嘛啊算了，为什麼我被袭击了呢？」

这个问题是非常理所当然的。
毫无理由地遭到攻击，即使对古蓝来说也是不能接受的吧。
如果想要和解的話，至少要把理由说出来呐。
伊莉丝理解了那个/他的意思，脸颊（因）难为情（而）染上绯红地说道。

「那个是……不好意思，（这是）所谓的，误会，认错人，的东西……」

「什麼意思？」

「一瞬间把古蓝大人当成我的重要之人的敌人了。但是，重新看的話一点都不像……真不好意思……事到如今才想到，把古蓝大人放在一边，真的十分抱歉……」

（因为）谈話的内容，古蓝似乎露出了稍微悲伤的表情，（他）用勉强能听到的极限音量喃喃自语着。

「在这种岁数，报仇吗……掌握那种程度的魔术也是因为如此吗……似乎过着每天呕心沥血（血反吐吐く）的日子呐……真是世事艰辛（世知辛い）啊。」

不仅鲁鲁，拥有超越人类身体能力的魔族的伊莉丝的耳朵也理所当然得清清楚楚地听到了那些話语，但是两人都默不作声。
古蓝之後浮现出爽朗的笑容说道。

「明白了！要是那样的話，没办法啦（ぜ）。这次的事情就当成是个不幸的事故吧！」

是个感觉不错的男人啊。
不管是鲁鲁还是伊莉丝，此时看着古蓝的（他们）打从心底如此认为着。

「如果那样的話，对我太有帮助了。谢谢，古蓝大人。」

伊莉丝如此说着低下了头。
古蓝，

「所以说没有（被这样说）的资格……」

（虽然）正说着这样的話，（但是）心情好像不坏。
觉得大概能无隔阂地离开遗迹了，鲁鲁放心了。

「既然和好了，暂时先回村子吗？也是为了这个丫头……伊莉丝着想呐。」

「嗯（あぁ）……对了，那个，小妹妹为何会在这种地方呢？」

鲁鲁回答了古蓝的问题。

「我在古蓝昏迷的时候打听过了，似乎是在追赶敌人的当口不幸的中了圈套导致在此长眠。好象不明白长眠了多久，但是调查那个也没用吧。（她的）父母似乎也早就不在了呐……暂时（因为）谈話关係变好了，我想让她在村子裡一起生活。」

没有一句話是谎言。
只是没有细说罢了。
古蓝似乎接受了那个说明似得点了点头。
伊莉丝什麼也没说，似乎（全部）交给鲁鲁（来说明）的样子。

「原来如此呐。圈套吗？嘛（ま），这样的遗迹中也有那样的东西吗？所谓的父母不在了的是指……那个吗，意味着能依靠的人也没有了吗？」

「嗯（あぁ）。」

「也就是说，对了。关係变好了的話，最好是住到离你近的地方比较好呐……去都市的話，也只能住到贫民窟去（スラム=slum）……」

（他）如此说着微微地远目了，也许是在挂心着年幼就失去父母，成为/拥有追逐着敌人的身世的（她）吧。
古蓝似乎人情味很浓厚呢/很有人情味呢。

在互相理解就像这样发展的时候鲁鲁说道。

「那麼（じゃあ），出發吗？」

「喔（おう）。」

「是（はい），那样做吧。」

於是鲁鲁他们三人，为了离开遗迹（而）拔腿前进了。

