本赛和预赛的热情不同。
到预赛为止，各种各样的选手们分开了四个竞技场，在混杂着鹅卵石的状态下进行着比赛，观众们也为了自己的选手，或者去看了有名的选手。有分散到会场的倾向。
但是，本赛的比赛全部都是在国营竞技场进行的，入场时会有很高的倍率，甚至接连出现无法进入的比赛者。
实际上，为了无论如何都想观看这场大选，为了参加比赛者而设立的专用观战席，参加竞技大赛的人也络绎不绝。
当然，因为是有生命危险的大会，所以应该修行之後再上场，但是即使如此也不能保证安全，为了大赛而牺牲生命。虽说是赌博也不为过。
至於王都中狂热的大会，高潮，那就是大赛的正式大选，这种热情也是可以理解的吧。

鲁鲁站在热气的中心，等待着对战对手的出现。
如果站在这裡举出与以前不同的地方的話，那应该就是对鲁鲁的欢呼声吧。
虽然也有骂人的声音，但那也是最开始与修伊战鬥之前不存在的，这无非是提高了知名度。
从某种意义上来说，就把它当作有爱的声音来接受吧。

在那之後，想到了这次的对战对手。
自从与修伊对战以来一直认为，鲁鲁的对战对手全都是实力者。
修伊是特级的，之後撞到的对手也不到那裡，都是高级的。
的确，这次的竞技大赛和往年相比，高级冒险者的数量很多，而且特级也可参加的新的比赛，所以不能说没有撞击的可能性。
不过，不管怎麼说，总觉得与鲁鲁的对战对手，实力者是否太过偏向了。
在预选赛第二回合之後，没有一个人碰到过未到上级的人，这在鲁鲁以外的出场者中是不存在的。
虽然不能说这不可能是概率性的，但是即便如此，他还是感觉不到是故意的。
而且这次的对手也是——

「哎呀哎呀……我，想弃权啊……」

当老人一边说这些話，一边出现的时候，会场被一片盛大的欢呼声包围着。
虽说对鲁鲁的应援声也不少，但从知名度这个意义上来说，这对这位在王都多年来冒险者面前的老人来说，恐怕是无法抗拒的吧。
而且听说他的功绩也非常大。
十年一度左右的时间裡，在王都周围好像有魔物集团發生，但他——乌威斯德・安格尔(ウヴェズド＝アンゲル)，以及他的氏族“绮污的真理”，每次都会参战。战鬥之後，通过大规模魔法的行使，将许多魔物沉没了下来。
氏族“绮污的真理”本身的歷史也非常漫长，这是冒险者工会成立後，在正式承认氏族这个团体存在之前就存在的有歷史的氏族。对於王都迪西埃尔特的魔法师来说，属於这个氏族是被认可为一流的魔法师而憧憬的氏族。
身为氏族族长的乌威斯德・安格尔，长着一副年老、白胡须白髮、身材瘦弱、看上去很弱的老人的容貌。
感觉不像是软弱无力的，用一句話来形容的話，就感觉他是个活泼的好人爷爷。
所以，他就是率领巨型氏族的特级冒险者之一，而且作为魔法师来说，他们是对无法再期望的顶峰下的存在。
如果不知道附随他的脸和名字的名声的人会歪着头怀疑吧。

但是鲁鲁不能够如此对待他，他没有能力做到。
的确，眼前的老人——乌威斯德乍一看就像个老头，魔力也不大，动作也显得相当迟钝。
年轻的时候或许确实是个强有力的魔术师，但现在已经快要隐居了，看起来不像是靠着过去的荣耀而活着的存在。

但是，鲁鲁曾经见过他这样的存在。
不，与其说是见过，倒不如说是知道。
是鲁鲁的——魔王的参谋，与他有着相似气息的魔导师，是他的亲信之一，穆特斯・伊伦努斯(ミュトス＝イレヌス)。
乌威斯德的气氛与他一模一样。

乍一看，并不是很坚强。
身体也乾枯了，好像已经感觉不到作为生物的活力了。
但是，那只是个拟态而已。
魔力变小，看起来弱小，是因为在他心中凝固到极限的魔力，随时準备投入魔法中，身体看起来很薄弱，对於身为极限魔法师的他来说，所谓身体能力并不是通过锻炼身体而得来的，而是通过大量的魔力和细致的魔力操作技术创造出来的。
乌威斯德和这麼可怕的魔导师很像。

这个事实让鲁鲁担心绝对不应大意。
所以，从他嘴裡说出来、想弃权之类的話，听起来也只是个玩笑而已。
鲁鲁对他说。

「年老的身体。虽然不是这样，但是你看起来似乎拥有让人想弃权的力量。与其这样倒不如……感到奋战的兴奋」

乌威斯德听了鲁鲁的話，笑了笑，一边抚摸着长长的白色下巴，一边歪着头。

「哎呀……这样的老人有多大力气啊……？看好了，年轻人。只要像你这样精通魔术，一目了然就能感觉到我拥有多麼微弱的魔力吧……不对吗？」

老人张开双手并说着，鲁鲁进一步加强了警戒。
不管怎麼想，这句让人大意大意的話语肯定是打算战胜鲁鲁的。
为了这个目的，他用了各种手段，而且为了不让对方察觉到，他注意到了各种各样的动作，果然还是让人想起了过去的穆特斯。
鲁鲁之所以能理解这一点，是因为鲁鲁学魔法的对象就是穆特斯，而且这个穆特斯也像眼前的老人一样狡猾，因为他是那种会招致对方疏忽大意的类型**老头。
鲁鲁一边想起那个曾经认识的人，一边说道。

「……我的熟人，有和你一模一样的老头……那时的经歷告诉了我能不要大意。虽然确实感觉不到魔力，但是我很清楚这是为什麼。所以我很明白你是个可怕的老头子……我不会大意的」

听到鲁鲁的話，乌维斯德睁大了眼睛，呵呵地笑了。

「和我很像的爷爷？……是个性格不好的老爷啊。不想见到啊」

在宣告这种事情的时候，一切果然都是为了诱惑大意而自认的陷阱。
然後，乌威斯德继续说道。

「但那样的話……已经隐瞒也没有意义了呢？呵呵呵……嘛，年轻人啊。别倒下哦」

就在下一瞬间，隐藏在老人身体深处的魔力漩涡散布在四周。
浓密，庞大的魔力。
在今世，鲁鲁所见到的魔力比任何魔法师都大，而且是浓重的魔力。
而且也感觉到了非常有攻击性的性质。
魔力对整个斗技场施加压力，剥夺了对魔力抵抗力低的人的意识。
比赛开始前，回过神来，会场的观众中大约有三分之一失去了知觉。

「真是个了不起的老头啊……」

「哼……我可不想被你这麼说。同样的事情——不，更大规模的事情，主要是可以做到的吧。修伊和优蜜丝曾说过，这世上竟然有拥有如此荒谬魔力的人，真是让人难以置信。我刚才说想弃权。那是认真的。想要和像你一样的怪物认真战鬥的笨蛋魔法师到底在哪裡。赶快跑回自己家裡盖上被子睡觉」

带着厌恶的神情说这种話，真的是这麼想的吧。
但是当然没有战意。
他的瞳孔裡充满着清晰的气势，从他全身都感受到了与刚才截然不同的强烈压力。

「如果可以的話，我也希望你能睡着……真的不弃权吗？」

「那才是，别胡说八道了。虽说如此，我也是氏族的族长。如果能展现出相应的战鬥暂且不论，如果避开了战鬥的話，对氏族的人们也感到抱歉。即便是魔物成群结队地前来，也从未感到过如此的恐惧，不过，这也是为了给自己一个能拿出真本事的好机会而战鬥的吧。放弃吧，让这个老人心甘情愿吧，年轻人啊」

「一般相反吧……给年轻人让路才是，老头子」

乌威斯德飞溅的魔力也卷入了鲁鲁的魔力，虽然比赛尚未开始，但舞台上却形成了紫色电卷起旋涡的龙卷风。
聚集在竞技场的观众们的兴奋也达到了最高潮的时候，期待已久的信号响彻了竞技场。

「……开始！」

然後比赛开始。

　◆◇◆◇◆

「他是有名的人吗？」

问了和伊莉丝一起看比赛的优蜜丝。
预选期间，在为氏族族长副族长设计的特别观战席上观看的格兰和优蜜丝，似乎也希望这次大选能够直接感受到会场的热情。每年，关於本赛的比赛都是在战鬥中进行的，似乎是打算在场馆席上观战。

「因为乌威斯德・安格尔的老头子是氏族“绮污的真理”的族长。很有名。一般的魔法幾乎都可以使用，而且据说还有自己的术式。話虽如此，好像还没有人看过那个……反正那个老头子找能用魔法战胜的人也很少找到」

「那就是说那个爷爷是世界上最强大的魔术师吗？」

「不，不是这个意思。在国内，除了宫廷魔术师长之外，或许是最强的，但是其他国家也有同等级的魔法师，除此之外还有独有的术式。因为在场的話，那个老头可能更差。不过，这要看那老头所持有的王牌所拥有的独立术式是怎样的了。在某些情况下，那个老头也不是最强的」

听到这个，伊莉丝肯定了乌威斯德的实力，作为魔术师是顶级的，但不知道是否是最强的，但也不是没有这个可能性，但是在现实中被看做最强的人之间，如果不试着战鬥的話，是无法理解的。
实际上可能更複杂。
下位的人赢过上位的人并不稀奇，而且经常说强者的地位是不断交替的。
即便如此，乌威斯德的强大力量也无疑在王都中谁都会承认的吧。
而且，伊莉丝也和鲁鲁一样，感受到了对乌威斯德的怀念。

「……跟穆特斯爷爷一模一样……」

平时是个好人，又像学者一样，是曾经的魔王亲信之一。
是个有点神经质，绝对不想让任何人碰自己的收藏品的老头子，但是他的实力是真实的，而且比这更重要，是以狡猾著称的人。
身为教师也很优秀，有很多人向他学习魔法，伊莉丝也曾接受过他的教导。
与这样的他很像，也就是说，那个乌威斯德就是个不寻常的人。
与其说是手段的好坏，不如说是比较重视结果的善恶感薄弱的人。
鲁鲁不是那种会使用缠绕手的类型，这样的对手不是很擅长。

但是，即使如此，鲁鲁也曾经是魔王。
凭藉其魔力和力量，率领魔族的他。
虽然他(老爷)跟过去的亲信很像，但她确信他(鲁鲁)不会输给人类的魔法师，所以伊莉丝相信鲁鲁的胜利而守护他。
