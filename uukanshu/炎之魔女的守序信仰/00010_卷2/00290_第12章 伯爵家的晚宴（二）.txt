怎麼办！怎麼办！怎麼办！

危机的警钟声在我的脑内响起。

为什麼他会在这裡啊。

我的身份要暴露了吗？

这个傢伙不会一直跟着我吧，为什麼会有这样的碰巧呢。

不……是我太不谨慎了，明明已经从大家那裡听说了伯爵邀请了多国的贵族，那麼他被邀请也是情理之中的事。

将脖子向后转了仅仅30度，我用余光微微向身後的高大人影瞥了一眼。

穿着白色燕尾服的他看上去是较为瘦长，和之前身着铠甲的感觉大不相同。

毕竟是可以挥动那样夸张的两把大剑战鬥的人，体质绝对不可能贫弱，但是倒是出乎意料的穿衣服显瘦型呢。

唔，不对……卡尔穿什麼衣服和现在的状况一点关係也没有。

问题是如果被发现了我该以什麼借口糊弄过去呢。

事实是我从修道院裡逃出来了，虽然一切都是那些想要我性命的混蛋们的错。

不过牵扯到魔女的事情当然不可能说出来。

“那个，真的没事么？”

卡尔的声音再次传入我的耳朵，他的声音充满着可靠感，一瞬间让我觉得即使转过头也没关係。

但是那是不行的，像他这麼正直的傢伙发现我在这裡的话一定会重新把我送回希尔芙利亚的，然後再向父王禀报自己所知的事情的来龙去脉，一滴不漏的。

其实父王应该根本没有让我回去的意思，毕竟假身份都做好了，还把风精灵项链送给了我，一副就此永别的感觉。

但是这个榆木脑袋肯定是想不明白的吧，用脚趾都能知道，这傢伙的脑子裡只剩下荣誉和正义了。简直和布雷尔卿如出一辙，对了……他们原本就是父子来着。

“抱歉，真是劳烦大人了，妾身没有关係。”

仍然以背对着的样子，我向卡尔道了谢。

“妾身？”

糟糕！习惯的坏处显现了。

“抱歉，是故意这样说的……咱这样的平民女孩子非常憧憬贵族大小姐来着。”

太悲惨了，比上次糊弄少爷的借口要更加残念了。

我才不喜欢做这种装模作样的事情啊。

“是这样啊……不过仔细一看你的发色很特别呢，有点像某位大人啊。”

“那一定是错觉，卡尔大人，这种发色在特罗洛普时很常见的。”

“我刚才有报上自己的姓名吗？不过声音也很像呢。”

“！！”

突然而来的紧张事态让我的神经就像搭错线一般，简直是错误频出。为什麼会连续犯这种白痴般的失误呢！

等等，冷静下来。越慌张越可疑……

尽量扯开话题，别陷入他的步调了。

“卡，卡尔大人的勇武天下无双。‘巨人之腕’的称号妾，咱在年幼时就听说了。”

“啊……但是我现在也不过22岁而已，讨伐巨人也只是两年前的事罢了。”

“……”

这傢伙难道是故意等槽吐。

“那个……可以请你转过头吗？”

卡尔的声音中夹杂的疑惑气味已经越来越重。

但是，转过来就玩完了好麼。

“咱的容貌长得很难看！怕惊吓到大人。”

“骑士是不会以容貌去区别对待女性的，而且刚才韦伯斯特家的少爷也说你很可爱来着。”

“您这样……会让妾身很为难的。”

我的声音已经带着强烈的拒绝之意了，这样他也应该放弃了吧。

“那真是太遗憾了……如果可以的话可以告诉在下小姐的芳名吗？”

搞定~~毕竟这傢伙性格太过於耿直，不会太欺负女孩子吧。

名字随便想一个。对了，之前给女装的少爷取得那个就好。

“爱丽……”

“啊！！克莉斯，你在这裡吗？让我找了好久啊。”

……

完了……

我掩面看向向我这边过来大声呼唤我名字的艾文少爷。

这个时机也太妙了吧。

为什麼刚才那个红毛混蛋缠上来的时候您不过来呢！！

“刚刚他说‘克莉斯’？”

事到如今就连这个榆木脑袋也应该明白了吧。

哈哈~~没错。妾身就是这个世界上最可爱的公主殿下~~

个鬼啊！！！！！

先逃走再说吧。

看准了空档我以疾风般的速度闪到少爷的背後。

“诶，發生了什麼吗？”

“少爷，请帮妾身挡一下。”

我在少爷的耳边用卡尔听不见的音量轻轻说着。

“唔，耳朵稍微有点痒呢，有必要靠这麼近吗？”

“先不说这个，其实这位大人是个变－态，不能让他听到，否则肯定会袭击妾身的”

“啊？”

“他刚刚还说要我卷起裙子给他看！”

“什麼！？简直岂有此理，连我都没看过的说。”

问题完全不在这裡吧，不过现在先不管这些小事了。

“呃，所以请少爷帮妾身挡住他。”

“欸，那个，这种事还是立刻告诉老爹比较好吧，让他把没规矩的客人请走就好了。”

“不行！会给伯爵大人添麻烦的，如果少爷告诉伯爵大人妾身就再也不理你了。”

如果伯爵大人亲自出手的话，我的身份可以说是暴露无遗，估计那时候这个女仆也就做不成了。

“唔，我明白了……就把这傢伙交给我吧。”

“果然还是少爷最可靠了~~爱你哟。”

“欸……”

太帅了，少爷，感觉都要被您攻略了。

那麼……我就先走一步了。

“请稍微等一下，克莉斯殿下。”

啊，完全被识破了吗？

不过已经无所谓了，只要不被你抓住就没有证据。

无视传向这裡的卡尔的呼喊声，我从艾文的身後冲向通往过道的小门。

幸好没什麼人关注这边的事，并没有引发骚动。

那麼，先往花园裡去吧。

同时，少爷张开双手挡在了卡尔的面前。

“喂，不能让你这个变－态过去。”

“变……态？”

“没错，你这傢伙休想对克莉斯出手！”

“请让开，少年，这裡面有误会。”

“什麼误会啊，这可是克莉斯亲口和我说的。”

“公主殿下还是老样子那麼棘手呢。”

“欸……公主殿下？”

“很抱歉，没时间和你磨蹭了，我也先走一步。”

推开眼前的少年，卡尔紧追其後。

“喂，等一下啊！”

少爷貌似也追了上来。

为什麼会变成这种状况啊！

他的脚程好快啊，我和卡尔的距离正在逐渐缩小。

以前一直没发现的，这靴子好难跑。

还是有点高跟的设计，虽然是很好看啦，但是现在却完全成了负担。

“公主殿下，请稍等一下，在下并没有恶意。”

“等个鬼啊！妾身绝对不要回去。”

“不，在下现在只想弄清楚殿下發生了什麼事而已。”

“所以说你们刚才一直就在说的‘公主殿下’究竟是什麼啊。”

保持着三人追逐的步调，位於最後的少爷对事情的始末却全然不知。

“算了。希尔芙，祝福之风！”

无法赶上卡尔速度的少爷咏唱了了风魔法。

以难以置信的速度，少爷冲上了墙壁展示了华丽的走墙技巧，然後猛踢天花板，落在我的面前。

喔~~实在是太厲害了，这技巧叫什麼来着。

三角跳？以前在格斗游戏裡用过。

不过在现在这种时候已经没时间去感叹了，卡尔幾乎已经近在咫尺。

“抓好哦，克莉斯！”

“难道说？”

又要像上次一样跳来跳去吗？

“呀啊！”

还没等我握住少爷的手，鞋跟便突然断掉了。

我也受到惯性影响狼狈地摔倒在地。

“啊啊，真麻烦，别乱动哟。”

少爷回转过身，迅速将我抱了起来。

然後再次向半空中迈出步伐。

比起之前的过於粗枝大叶，这次他的动作要平稳很多、

“妾身不重吗？”

“没关係，有魔法的辅助，而且克莉斯原本就很轻，没关係啦。”

“嘿嘿~~”

“那傢伙已经被甩得远远的了。”

“嗯，不过这样的距离还不够，尽量往外面跑吧。”

“真的要做到这种地步吗？”

“嗯？”

“那个人看上去不像是克莉斯说的变－态啊，而且好像有话要说的样子。

嘛，当然我是相信着克莉斯的。”

“谢谢……”

迟疑了一会，有点乾涩的喉咙裡却只能發出这两个音节。

欺骗少爷的信任多多少少还让我有些愧疚。

或许这样的逃跑并没有什麼意义吧，卡尔之前也帮助过我，和那些想要取我性命的魔女杀手并不相同。

“请放妾身下来吧，少爷。”

“欸？”

“停下魔法吧，接下来就交给妾身就可以了。”

“嗯，我知道了。”

看起来是明白了我的想法，艾文跳向了平时练剑的花园，然後在中央的草坪处将我放了下来。