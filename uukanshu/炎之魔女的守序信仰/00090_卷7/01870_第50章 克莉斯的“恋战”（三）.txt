街道上散着魔法灯的微光，随着逐渐入夜，灯火的亮点也微微变得黯淡了一点。

不愧是世界上最大帝国的首都，在特罗洛普可是没有这麼多魔晶体用来维持公共设施呢。

即使是这个时候了仍然有店铺在营业……

也时不时有人们经过街道，其中不乏成对的恋人。

巡逻的骑士们正在交换班的时候，刚交接过结束了工作的一名骑士取出了怀中的酒壶靠在城墙边上小酌了起来。

注意到我的身影后，他来到了面前。

“小姑娘，这麼晚了一个人在外面很危险哦~~欸，这身制服，是学院的学生啊。”

“欸……”

“嘛，总而言之小心一点哦，在下送你回去吧……我以前也是那裡的学生呢。”

骑士的搭话将我从思绪中拉了回来。

这裡一下子也不知道怎麼回答，比起愿不愿意来说根本没搞清楚情况还比较合适。总之，这种状况下先拒绝就好了。

“啊，谢谢，但是不必了，妾身一个人就可以了。”

我尽量礼貌地弯下了身，表明了谢意……但是果然这时候还是想一个人呆一会。

“不必客气，送可爱的小姐回家也是骑士的责任啊。”

“真的……不必了，非常感谢你。”

虽然这麼说，但是骑士依然向我伸出了手，是因为喝了点酒吗。显得不会看气氛了，但是为这点小事就出手实在太夸张了……

就在我犯难的时候，不远处传来了急促的靴子踏地的声音。

声音越来越近……有人向这裡跑过来。

“喂！幹什麼呢，你这个变－态骑士！！”

咚的一声，铠甲貌似被什麼钝器般的东西击中那样發出了一声闷响。

回过神的时候骑士已经被击倒在地，借着酒精他一下子就昏睡了过去。

“欸？奥菲莉亚……”

“人家也在哦……”

赫卡忒慢了一步也跟了上来。

“你们……”

“哼……话说在前面，我只是路过帮助下迷路女生啦，和朋友什麼的一点也不沾边啊。”

奥菲莉亚抱着两手将视线撇开。

嘛，我是清楚你想表达什麼啦，但是……

“……这位骑士，是好人哦。大概只是喝了点酒想送妾身回去而已……”

“欸！？怎麼会……”

她一下子就放下了架子，双腿也稍微摩擦着看着面前倒在地上的骑士不知道怎麼办。

“呼呼……呼。”

我也不禁笑了出来~~

“有，有什麼好笑的啦！”

“哈哈，抱歉……觉得奥菲莉亚经常做这些冒失的事情呢……”

“……哼，那又怎样啊，反正我们已经不是朋友了。”

看来她依然坚持着那件事呢……

“那麼，作为义姐妹如何~~”

我笑着回答道，对此她稍微迟疑了一下。

既然看到我陷入困境依然来帮忙，那麼还是有对话的餘地的吧。

“……也不是不可以啦……因为你这样实在让人不安啦，别误会哦。”

果然如此，奥菲莉亚的脸变得有些红，即使依然交叉着双手，但是明显也让步了一些。

仅仅是吵架那样的事情……也不需要太过在意吧。

出生入死的同伴可没有绝交一说呢。

“嗯，请多指教啰~~奥菲莉亚姐姐~~”

我抬高了声调~~微笑地说出了这样的称呼。

对於我的话她的脸红变得更加明显了起来……

“啊啊，还是按照以前的吧！叫名字就好了……”

“欸，可是我们已经不是朋友了呢~~奥菲莉亚姐姐~~?”

我故意又将声线调高~~在奥菲莉亚的名字後發出了有些撒娇般的娇音。

“果然还是不行！！”

“喂……你们两个再这样人家就先回去了……”

赫卡忒看着这边冷冷地说道。

…………

……

莲和赫卡忒的住所是前幾天他们租下的一间面包工坊，诺布尔学院位於贵族区的边境，然而这所麵包坊仅仅是位於学院大门向西转过街角的地方，路程还不到五分钟。

之前莲给的耳坠通讯器丢了呢，想问他再要一个来着……但是莲说他只携带了一个备用的出门。现在的话没办法随时联繫了。

“莲是打算经营面包工坊……吗？”

我跟着开门迎接我们的莲一起走进室内，裡面还残留着麵包的香味。

“才没有……只是原本这所工坊的主人要回家乡所以便宜出租了，我因为经常出入公会，提供这样的房东给我也算是他们给的优惠吧。”

是吗，因为很优秀的冒险者也能为公会带来很大的利益，所以住房上也会尽量给与便利吗？

“……莲应该很出名了吧。”

莲和赫卡忒一直靠着解决超高难度的任务而维持着衣食无忧的生活，单单解决高难度任务的话只要完成数件就能成为公会的头牌了，他们两个应该已经是B+级的顶级冒险者也说不定。

“嘛……我一直用外号，也一直单独行动，应该不算多显眼吧。”

记得莲的外号好像是什麼狼来着……呼呼，闷声~~

先不说这个了，这间屋子需要打扫呢。

我看向周围，空空荡荡的，货架上还残留着麵包屑。

裡面的房间也是，乱糟糟的……箱子什麼的堆得到处都是，还有些发霉的气味。

简单来说……糟糕透顶……

“莲……你就让赫卡忒住在这种地方吗？！”

裡面姑且是有两张床啦，但是却似乎没怎麼收拾好，这傢伙真是出乎意料的过分呢。

总感觉他故意冷淡赫卡忒一样。

“……没什麼哦，这是莲和人家的家~~怎麼样都无所谓。”

“别说的那麼奇怪，只是个暂时的据点罢了。”

对於赫卡忒的直球，他习惯性地避开。

“莲……呜呜。”

结果龙少女的眼睛又噙着泪了。

她的嘴唇也微微颤抖着……

“你这傢伙别太过分哦！！”

奥菲莉亚在我前面就忍不住发飙了。

“……就是就是！莲有时候太坏心了，明明平时就完全不是那样。”

对待其他女孩时就很温柔的说……为什麼偏偏对一直恋慕自己的女孩这样冷淡啊。

要是少爷也这样的话，我大概早就受不了了。

不……那样的话大概不会喜欢上他吧。

就是因为艾文他，太温柔了。

唉，不知不觉又联繫到自己身上去了啊。

我拍了拍双颊，让头脑清醒一下……

“……嘛，那麼就稍微打扫一下吧。”

莲拿起了放在墙边的扫帚，巧妙地躲过了追问。

“妾身也来帮忙吧……对了，没什麼古代科技能用吗？”

“怎麼可能有……以前我们的建筑都是自动清扫，根本不必管的。”

居然还有些自豪地微微扬起嘴角。

“所有造成了你这种懒惰的个性吗！！”

对於我凌厉的吐槽，莲装作没听见一样保持着握着扫帚的挺直站姿。

“你们在说些什麼啊，别偷懒哦……我也可以用水魔法帮忙的。”

奥菲莉亚在指尖聚集了水球。

“莲……”

“所以别一副快哭出来的样子啦，干点活就能都忘记啦~~赫卡忒~~”

“呜呜……好吧。”

奥菲莉亚的水魔法用的貌似不是很熟练，中间两次没控制住把大家身上都弄湿了。

赫卡忒的元素魔法貌似破坏力很大……所以莲禁止她在平时使用。

虽然身上潮潮的有些不太舒服，但是现在打扫的过程还算比较愉快……至少自己的身边还有这些同伴，想到这裡便让我稍微安下了心来。

…………

“总算乾净了呢……哈啊。”

“虽然还是空荡荡的。”

“之後买一点家具吧……”

“嗯！！”

“哈哈~~”

“哈哈哈~~”

“哼……”

“呼呼。”

结束之後，我们四人一下子散开躺倒在地上累的动不了了。

真是过分……打扫伯爵家也没这麼累呢。

一开始只是打算稍微弄乾净一点，结果到一半的时候就觉得不做到完美不行，於是把边边角角都擦到了发亮的程度……

其他人也被我的幹劲调动幾乎都用尽了全力。

“想洗个澡呢……有浴室吗？莲……”

“怎麼可能有，这原本可是平民的家啊，想洗澡的话去後院的水井吧……”

手抵在额头上，看上去非常疲倦。原来古代人大扫除之後也会感觉到累啊……

“唔……这样啊……”

“不去的话就只能忍耐了……”

“不要……好难受。”

身上黏黏糊糊的，而且因为打扫衣服也变脏了，可以的话想洗一下。

“克莉斯……衣服放在篮子裡吧，我一会会洗的。”

“欸！？可是……莲在这裡……唔。”

不自觉说出这句话後连我自己也吃了一惊。

我什麼时候开始变得这样在意男性的视线了呢……

以前对待那个黑妖精哥哥利贝尔的时候还不觉得只穿有什麼好害羞的，但是现在如果被男性看到肌肤就会觉得全身都在升温一样……

“……我先到客厅吧，半个小时後再进来。後院没有其他人，你们安心用就好了。”

莲说完坐起身离开了卧室。

“嘛，我倒是不介意……”

“不要，你们两个对莲的视觉伤害太大了……尤其是克莉斯！才一年而已你这胸部是怎麼回事啊！为什麼又长大了一些。”

我把衣服褪去放到篮子裡後，赫卡忒貌似又产生新的不满了。

嘛，虽然我觉得她这样已经很可爱了……不过对於大多数女生来说还是渴望丰满一些的身材吧，就连龙族也不例外呢。

“啊哈哈，大概正處於发育期吧。”

“人家不想听这种借口，叛徒！”

别那样怨念地盯着我啦，有时会涨的难受也是很烦人的，被衬衣摩擦到那裡也会很疼。

由於只有一个水桶就先让奥菲莉亚和赫卡忒先清洗身体了。

本来还想给她们两个把水加热一下……但是两人都拒绝了。

“不需要，人家是龙……体质比人类强多了。”

结果奥菲莉亚一听也开始逞强起来。

“我也不用……对於格闘家来说这点磨炼算不了什么。”

感冒了我可不管哦……即使我现在只穿着被夜晚的微风拂过都会感到有些冷呢。

稍微过了一会，奥菲莉亚发着抖并将装着衣服的篮子拿了过来……然後一下子钻进了被窝中。

“哼……人类就别逞强啦~~”

赫卡忒挺着小小的胸部得意得笑了。

接下来就轮到我了吧……身上的汗已经快干了，但是不洗一下还是非常难受。

“双月的位置确实非常接近了呢……”

到了4月10日就会近乎重叠。

那一天被冠上月之王的学生将可以指名自己喜欢的人一起跳舞。

距离现在还有一个礼拜左右……

虽然不知道该怎麼办？

但是我不想放弃……我想在双月重叠之刻，在希尔维娅公主的面前将对少爷的心意说出来。

炎魔法将井水加热……

即使这样依然觉得有些冰冷。

“欸……不可能的吧……”

月之王什麼的……我也不是擅长做那麼显眼的事的人啊。

只好默默地叹了一口气。

“什麼不可能？”

“欸？”

背後传来了声音……男人的声音。

“哇啊啊！莲！！你怎麼会在这裡！赫卡忒没阻止你吗！？”

我急忙用毛巾掩住了上半身，坐在小凳子上缩成一团。

“放心啦……我转过身去了，那两人的话……现在睡熟了。”

“有时间跟妾身解释这个！快滚回去！”

随手将一块石头扔了过去，但是好像被他灵巧地躲过了。

“……先听我说啦。”

“欸？！”

“正好趁她们两人睡着我才打算说的，否则解释起来太麻烦了。总之，我可以帮助你哦，克莉斯……”

他的声音变得低沉了下来。

“帮助是指的是……”

我放下手掌，散掉聚在那裡的魔力。

然而青年只是静静地回答了我……

“当然是……帮你赢得你最想要的东西。”