“维德米尔……”

嘴唇轻轻拨动，念出了这个词语。

平原上的魔物并没有什麼攻击性，看见我也只是停了一下然後又蹦蹦跳跳地离开了……

看到它们这副可爱的样子总觉得是不是魔物还很难说，大概只是我没见过的动物而已吧。

在罗洁艾尔的船上乘了很久，尽管现在城市远远地在视野之中，但是那肯定是超过我预想的距离吧。

快艇的速度能达到40节以上，虽然他通过魔法之力行驶的船应该没这麼夸张，但是也跑了很远一段了。

“呼……该怎麼回去啊。”

难道要徒步吗？

我望了望因为阳炎而变得有些模糊的都市……

不要啊……走幾十公里绝对会死的，绝对啊……会被晒成克莉斯干的。

美少女就要这样变成干尸了吗。

来个好心的开车的土豪救救命啊ORZ……

说实话现在就已经身心俱疲了，失去了魔女的力量之後就连体力也下降了不少。

找了个有树的阴影处。

躺在草地上清新的泥土味扑鼻而来，微风拂过青草被吹得發出哗啦哗啦的响声……

感觉世界就剩下我一个一样……

哈哈~~如果那样还真是讨厌。

阖上了眼睑，想稍微睡一会。

真的只有我一个人被召唤到这个地方了吗？

少爷他呢……如果原因是魔女的话会不会有其他人也被召唤来这裡？

还有三分之一的世界究竟是什麼意思，那个边界的外面还有其他大陆吗……

一旦遮蔽了视觉脑子内想的反而更多了。

扑~~

“呀啊！！”

有什麼东西爬到脸上来了，是个毛茸茸的什麼玩意。

我坐起身来把趴在我脸上的东西取下来然後睁开了了眼……

“兔子？”

我看了看眼前捧在手上的东西。

白色的毛，长长的耳朵向两边垂下，不过身上有着一道红色的印记，是被人刻意印上去的吗？不然显得也太不自然了。

但是这东西……

“好可爱啊~~”

不由得紧紧把它抱在怀裡。

“咕咕！！”

被拥在胸口的兔子發出了不满的叫声。

“啊，对不起……”

一时没注意，但是那个双耳垂下来的样子真的好可爱啊，我赶紧蹲下身子将它放回地面。

不知道这个世界有没有白天黑夜之说……大概想多了，哪个世界都有白天黑夜的吧。

证据就是天空中的太阳还是有些晒呢。

总之，还是尽快赶路吧……可能要走上大半天呢，但是现在也没别的方法，尽量沿着河走吧，这样也不至於会被晒成干呢。

“bye~~bye，兔子先生~~

我转身像垂耳兔挥手告别後，径直向着远处的城市出發了。

到了晚上各方面都会变得比较麻烦，还是趁这个时间先回都市裡……

“呜呜！”

现在身上也没有值钱的东西……就算回去也只能沦落街头吗？

还是说该先找个能打工的地方呢。

好麻烦……别让前NEET族考虑这些事情啦！

“呜呜！！”

嗯？脚边痒痒的，刚才那只兔子又跟了上来……

“欸？兔子先生想要跟妾身一起吗？”

“呜呜！”

它奋力地上下点着头。

能听得懂我说的话吗？

“不过同伴的身边没关係吗？你也是有自己的家人的吧……呀啊！知道了！带你一起可以了吧，别舔妾身的脚！好痒！哈哈哈！！”

本身我是不太喜欢宠物的，不过如果不是狗的话还好吧。

以前被狗吼过，现在看到就不舒服。

“那麼就快点出發吧~~对了，既然是同伴了要给你取个名字吧……嗯，该叫什麼好呢？”

就在我认真地考虑它的名字时，垂耳兔發出一声尖锐的鸣叫。

“！？”

它的身体逐渐长大，变成了1.5公尺身高的巨大兔子，身上的那块红色印记也变得更加明显了，仿佛燃起的火焰一般。

“唔……”

“呜呜~~！！”

开玩笑的吧……就算在异世界这也太夸张了点。

这麼大的兔子，是从月亮上来的吗？

在我惊讶不已的时候，兔子先生俯下了身体。

“……是让妾身乘上去的意思吗！？”

“呜！”

它再次点了点头。

既然如此我也不客气了，确实我一个人走回去非常困难。

以自己的身高要乘上去还是很费劲，它耐心地等着我坐稳。

带着红色烈焰的巨大兔子，是做梦的话真想敲一下自己这爱妄想的脑袋……

不过既然是这幅样子那麼名字就已经决定了~~

坐好之後我轻轻抚摸着它的脑袋~~

“……那麼就叫你红狼了……唔……”

“咕咕……”

诶？不满意吗？

“别这样哦，这可是个很强的人用过的名字来着，已经成为传说的名字啊，并不是说兔子先生是狼之类的……”

“呜呜！！！”

听到我说了很强之後它立刻就兴奋了起来。

大概意思是那样的话就可以接受了吧。

明白了，表示否定的时候就“咕咕”叫~~赞同的时候就是“呜呜”地叫啊。

“那麼出發吧~~红狼！呜呜！！”

我也学起了兔子的叫声。

“呜！！”

“啊啊……”

太过於兴奋的红狼一跃而起，我慌忙紧紧抓住了它。

落地之後我狠狠地被颠了一下……

“平稳点跑啊……红狼！”

“呜呜！！”

像越野车那样在平原上掀起长长的尘土带。

以这个速度大概一个小时就能到了吧……

但是我能平安无事吗……

…………

……

“哈啊，屁股好痛……”

我揉着一路颠过来受了不少罪的臀部。

红狼已经变成了小小的样子跑到了我的肩膀上……

说实话有点重，但是毕竟这麼快带我回到了城市就别计较这些了。

太阳已经下去了一点，但是街上的人群依然非常繁忙。

现在还没没到下班的时间吧。

我带着红狼漫无目地晃悠在大都市裡。

一家店外的时钟显示的是16时40分。

太奇怪了，这个地方……

每个人的表情都仿佛是一致的。

我努力地搜索一个有笑容的人……

“呀……对不起！”

不小心撞到了一位穿着制服的OL。

我急忙弯下身道着歉……

“……没关係。”

她只是冷冷地回了一句，头也没回地快速离开了。

欸……这是很奇怪的事情吗……应该还算普通吧……

太久没有呆在这种现代化的城市裡，再加上我原本只是个NEET根本就不明白这样的反应究竟是不是一般人的做法。

但是如果在阿尔克纳拉的话大家不小心撞到了一般都会搭上几句话的。

嘛，尽量还是别惹麻烦吧。

我就这样一直选着人较少的街道走着。

在交叉路口的地方有一个标有地图的铁牌。

那裡标注着我现在的位置……现在是位于东区的边上……

一开始被召唤的十字路口应该是不远处的第四号商店街中心。

先回那裡吗？还是说先找个地方打工赚点钱呢……

“果然很奇怪……”

即使在出售电器的街道上，橱窗裡的电视机放的节目也只有新闻。

面无表情的主持人没有抑扬顿挫地念着稿子……

这傢伙居然还能主持新闻节目啊，感觉连一点生气也没有。“将这个没有任何生气的国家染上您的颜色……”

“！！”

回想起之前罗洁艾尔说过的话。

这个国家的人们是缺少生气吗？

人们表现得完全像NPC那样，只向着目的地前行……

就连孩子们也是，好像是一个班的小学生队伍，排得井然有序，就连私语也没有，等到信号灯变绿时穿过了马路

我搜索着街道的店面，如果可以的话本来还想尽量寻找白天工作晚上可以休息的店呢，餐饮店的话应该可以包一餐吧。但是现在连一家写有招工广告的店都没有……

人员都饱和了吗？

就连门口扫地的老爷爷看上去也是异常的卖力。

虽然这样也很好啦，但是总感觉哪裡不对。

缺少生气究竟是什麼意思……为什麼变成这麼奇怪的状况。

“呜呜！”

“怎麼了？红狼……”

右肩上的红狼突然叫了起来，然後一溜烟从我的肩膀那裡跳离向着前方跑着，就像发现了什麼一样。

“等等……啦！！”

无奈我也只能跟着它的方向向前跑，现在屁股还是很痛啦，慢一点啊。

兔子跑起来是这麼快的吗？刚才巨大化姑且不算，还以为现实中的兔子跑得快只是在童话故事裡呢。

奋力地追上去，真痛恨平时自己喜欢穿带跟皮鞋的兴趣……明明旅行了一年还一直改不掉。

但是即使这样跑着也没什麼人看过来，最多也就是瞥了一下再回到原本的样子。

穿过两条街，我们向着小巷子裡跑去。

“红狼……呼呼……你究竟要去哪？妾身……不行了，哈啊……呼……”

体力弱了很多我只能按着膝盖吁吁喘气。

然而兔子先生在小巷的出口也停住了……

抬头看到的是突然出现的高大建筑。

“……教堂？”

十字架高高装饰在建筑的上方，教堂四周是空空的一片。

这种都市裡居然还有这麼大的教会……

门口的大门露出了一道小缝隙，红狼从那裡钻了进去。

哎呀，这下看起来不进去不行了。

不过教会的话可以免费提供吃饭和住宿的地方吗？

总之先进去问问看吧。